# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.2](https://gitlab.com/ccrpc/collaborative-engagement/compare/v0.1.1...v0.1.2) (2020-08-27)

### [0.1.1](https://gitlab.com/ccrpc/collaborative-engagement/compare/v0.1.0...v0.1.1) (2020-08-26)


### Features

* **frontend:** add initial Jitsi integration ([b59fe50](https://gitlab.com/ccrpc/collaborative-engagement/commit/b59fe503794f0fa3479cf44da610f468bfc1b8bb)), closes [#27](https://gitlab.com/ccrpc/collaborative-engagement/issues/27)

## 0.1.0 (2020-08-20)


### Features

* **Access Score:** Access Scores are calculated for each intersection ([aa5bfe3](https://gitlab.com/ccrpc/collaborative-engagement/commit/aa5bfe3010afa250442ccdc0d2bfe8aac73f45ef))
* **access score survey:** access score survey packages form data as JSON ([6a0e970](https://gitlab.com/ccrpc/collaborative-engagement/commit/6a0e970090c19e787e17eaa8465d78303bb5b3d8))
* **Access Score survey:** Added real intersection data to the map ([1281608](https://gitlab.com/ccrpc/collaborative-engagement/commit/128160849bb9a16cf222d4293ddec421bdb6e4f1))
* **Access Score survey:** Added real intersection data to the map ([e3c8107](https://gitlab.com/ccrpc/collaborative-engagement/commit/e3c81078c4c19f04946596517c96c743f0972c5a))
* **Access Score Survey:** Added promoteId property to intersection layer ([ca8c4b1](https://gitlab.com/ccrpc/collaborative-engagement/commit/ca8c4b1d15b2f0ac70db481f4152f7377c7d8f68))
* **Access Score Survey:** Added promoteId property to intersection layer ([312146b](https://gitlab.com/ccrpc/collaborative-engagement/commit/312146b00a1c47081cdee5d9d692756a638c486a))
* **Access Score Survey:** coordinates of selected point is collected by act ([08f8ada](https://gitlab.com/ccrpc/collaborative-engagement/commit/08f8ada2f91dd7c4aa09073b0e0d698b1e1eb787))
* **Access Score Survey:** WIP: added map component and survey activity ([7e08cff](https://gitlab.com/ccrpc/collaborative-engagement/commit/7e08cffee9ae5fd229358a6463e92cd7d82615ee))
* **Access Score Survey:** WIP: configureing GIS styles in main activity ([4ef3196](https://gitlab.com/ccrpc/collaborative-engagement/commit/4ef3196144215e669f91295403782af44119350d))
* **Access Score Survey:** WIP: implementing data pipeline from form to db ([63b5a80](https://gitlab.com/ccrpc/collaborative-engagement/commit/63b5a80db31b35eb4239a802a72434d3c12b287e))
* **Access Score Survey:** Wrote utility function to calculate access score ([2b79e26](https://gitlab.com/ccrpc/collaborative-engagement/commit/2b79e262977a4553f7aaab2944366cf2abeb6783))
* **Access Score Survey Activity:** created entry form for survey ([da026f0](https://gitlab.com/ccrpc/collaborative-engagement/commit/da026f04d0d48288a251d56b31750863bb27843e))
* **Access Score Survey Activity:** improved popover interface ([a7db28c](https://gitlab.com/ccrpc/collaborative-engagement/commit/a7db28c01b71b4dc2a725d69e71870365f84fde7))
* **Access Score Survey Activity:** set map to fill its parent container ([b128abe](https://gitlab.com/ccrpc/collaborative-engagement/commit/b128abe2c139aa3ccceb8911881402f63382d1c4))
* **accessscore:** additions to map ([6272de7](https://gitlab.com/ccrpc/collaborative-engagement/commit/6272de73207715387c0e8b3b20ad644d238f9591))
* **App root:** Refactored to allow dynamic change in activity ([ad97000](https://gitlab.com/ccrpc/collaborative-engagement/commit/ad97000c9092c6cf25f2e795f843cf8f1c3603b7)), closes [#13](https://gitlab.com/ccrpc/collaborative-engagement/issues/13)
* Added Lobby page ([ca50a67](https://gitlab.com/ccrpc/collaborative-engagement/commit/ca50a67bfe497e6b9efdef3213913c74f56b8f1b))


### Bug Fixes

* **access score survey:** map is now rendering test style ([3a5e010](https://gitlab.com/ccrpc/collaborative-engagement/commit/3a5e010598394ffc36f7a61849f410f8a9aa68c3))
* **Access Score Survey:** Popup closes upon submission ([fb946ec](https://gitlab.com/ccrpc/collaborative-engagement/commit/fb946ecaa38eaab160c094be36bf381bc6315db2))
* **app-root:** small fix ([a0729bc](https://gitlab.com/ccrpc/collaborative-engagement/commit/a0729bc5e6bb4f3d99b0f2b28323f443a952faaf))
