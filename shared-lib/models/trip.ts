import { IComment } from "./comment";
import { IDestination } from "./destination";
import { IIntersection } from "./intersection";
import { IScore } from "./score";
import { IUser } from "./user";

export interface ITrip {
  _id: number;
  dateCreated: Date;
  dateModified: Date;
  user: IUser;
  comment: IComment;
  origin: IIntersection;
  destination: IDestination;
  scores: IScore[];
}
