import { ITrip } from "./trip";
import { ModeTransportation } from "../choices/modeTransportation";

export interface IScore {
  _id: number;
  modeTransport: ModeTransportation;
  score: number;
  trip: ITrip;
}
