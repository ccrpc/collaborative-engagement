import { ITrip } from "./trip";
import { IUser } from "./user";
export interface IComment {
  _id: number;
  text: string;
  datePublished: Date;
  dateModified: Date;
  poster: IUser;
  trip: ITrip;
}
