import { IComment } from "./comment";
import { ITrip } from "./trip";

export interface IUser {
  _id: number;
  username: string;
  device_id: string;
  comments: IComment[];
  trips: ITrip[];
}
