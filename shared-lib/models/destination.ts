import { Point } from "geojson";

export interface IDestination {
  _id: number;
  type: string;
  category: string | null;
  name: string;
  geom: Point;
}
