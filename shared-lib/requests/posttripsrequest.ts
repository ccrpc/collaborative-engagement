import { Point } from "geojson";
import { IScore } from "../models";

export interface IPostTripsRequest {
  commentText: string;
  username: string;
  device_id: string;
  origin_id: number;
  destinationPoint: Point;
  destinationName: string;
  destinationType: string;
  scores: IScore[];
}
