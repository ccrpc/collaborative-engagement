export const enum DestinationType {
  ARTS_ENTERTAINMENT = "art",
  GROCERY = "grocery",
  HEALTH = "health",
  JOB = "job",
  PARK = "park",
  PUBLIC_FACILITY = "public",
  RESTAURANT = "restaurant",
  RETAIL = "retail",
  SCHOOL = "school",
  SERVICE = "service",
}
