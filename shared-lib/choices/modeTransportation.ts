export const enum ModeTransportation {
  WALK = "walk",
  BIKE = "bicycle",
  BUS = "bus",
  DRIVE = "car",
}
