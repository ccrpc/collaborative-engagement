SELECT _id, ST_AsText(geom), type, name, category
FROM public.destination
WHERE name IN (
SELECT name
FROM public.destination
GROUP BY name, geom
HAVING COUNT(name) > 1
)
AND geom IN (
SELECT geom
FROM public.destination
GROUP BY name, geom
HAVING COUNT(name) > 1
)