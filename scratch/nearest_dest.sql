SELECT _id, name, ST_TRANSFORM(ST_SetSRID(ST_GeomFromGeoJSON('{
  "type": "Point",
  "coordinates": [
    -88.28310191631317,
    40.13175998487165
  ]
}'),4326), 3435) <-> geom::geometry as dist
FROM public.destination
ORDER BY dist
LIMIT 1



SELECT _id, name
FROM public.destination
ORDER BY ST_TRANSFORM(ST_SetSRID(ST_GeomFromGeoJSON('
{
  "type": "Point",
  "coordinates": [
    -88.28310191631317,
    40.13175998487165
  ]
}
'),4326), 3435) <-> geom::geometry
LIMIT 1