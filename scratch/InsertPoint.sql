INSERT INTO
"trip"("origin_id", "destination", "modeTransport", "destType", "score", "userAccessScore_id")
VALUES (11506,
  ST_SetSRID(ST_GeomFromGeoJSON(ST_AsGeoJSON(ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON('{"type":"Point","coordinates":[-88.420792,39.974997]}'),4326),3435))),3435)::geometry,
  '3',
  '5',
  63,
  84)
RETURNING "_id"