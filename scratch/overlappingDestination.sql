SELECT A._id, A.type, A.category, A.name, T.destCount
FROM public.destination as A
INNER JOIN (
	SELECT geom, COUNT(_id) as destCount
	FROM public.destination
	GROUP BY geom
	HAVING COUNT(_id) > 1
) AS T
ON A.geom = T.geom