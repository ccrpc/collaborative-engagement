SELECT user_access_score.origin_id as origin, trip.geom as destination, trip."modeTransport", trip.score, comment.text
FROM public.trip
INNER JOIN public.user_access_score ON trip."userAccessScore_id" = user_access_score._id
INNER JOIN public.comment ON user_access_score.comment_id = comment._id


SELECT trip._id as "tripId", user_access_score.origin_id, trip.geom, trip."modeTransport", trip."destType", trip.score, comment.text
FROM public.trip
LEFT JOIN public.user_access_score ON trip."userAccessScore_id" = user_access_score._id
LEFT JOIN public.comment ON user_access_score.comment_id = comment._id