SELECT _id, ST_AsText(geom), type, name
FROM public.destination
WHERE name IN (
	SELECT name
    FROM public.destination
    GROUP BY name
    HAVING COUNT(name) > 1
)
ORDER BY name ASC