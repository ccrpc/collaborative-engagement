SELECT "user"."username",
"user"."device_id",
"score"."score",
"score"."modeTransport",
"destination"."name" as "destName",
"destination"."type" as "destType",
ST_MakeLine("intersection"."geom", "destination"."geom") as "path"
FROM public.score
INNER JOIN public.trip ON score.trip_id = trip._id
INNER JOIN public.user ON trip.user_id = "user"."_id"
INNER JOIN public.destination ON trip.destination_id = destination._id
INNER JOIN public.intersection ON trip."originIntersectionId" = intersection.intersection_id