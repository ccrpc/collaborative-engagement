# Front-end Documentation
Last Updated April 21, 2021
----

## Introduction

This project contains 7 main page:

- [ ] 1. Video: Welcome and demo step 2

- [ ] 2. Rate your transportation access from home

- [ ] 3. Video: Access Score and demo step 4

- [ ] 4. Activity: Access Scores Evaluation

- [ ] 5. Video: Demo step 6

- [ ] 6. Current and Future Mode Use

- [ ] 7. Video: Thank you and contact info

When the user requests the [main page](http://cet.ccrpc.org.localhost/), they will be directed to the welcome page, where they will present an introduction video.

## Test work flow
the main website page:http://cet.ccrpc.org.localhost/
For local test, simple run:
```
yarn start
```
## Diagram
#### Work Flowchart:
![Work Flowchart](./src/assets/image/CET-work-flow.svg)


#### Components Structure Diagram:

![Component Structure](./src/assets/image/CET-frontend-structure.svg)

## Important Components

- [`ce-root`](src/components/ce-root)

- [`ce-map`](src/components/ce-map)

- [`ce-pretest`](src/components/ce-pretest)

- [`ce-pretest-form`](src/components/ce-pretest-form)

- [`ce-pretest-view`](src/components/ce-pretest-view)

- [`ce-access-score`](src/components/ce-access-score)

- [`ce-access-table`](src/components/ce-access-table)

- [`ce-destination`](src/components/ce-destination)

- [`ce-destination-form`](src/components/ce-destination-form)