import { h } from "@stencil/core";

export function getMap(id?: string): HTMLGlMapElement {
  return document.querySelector(id ? `gl-map#${id}` : "gl-map");
}

export function getThumbnail(item) {
  if (item.image)
    return (
      <ion-thumbnail slot="start">
        <img src={item.image} />
      </ion-thumbnail>
    );
}

export function toArray(value: string | string[], sep = ",") {
  if (!value) return [];
  if (Array.isArray(value)) return value;
  return value.split(sep);
}

// Based on: https://davidwalsh.name/get-absolute-url
export const getAbsoluteUrl = (function () {
  let a: HTMLAnchorElement;
  return function (url: string) {
    if (!a) a = document.createElement("a");
    a.href = url;
    return a.href;
  };
})();

// Calculates the access score for all modes of transport at a given intersection
export function calculateAccessScore(
  modes_transport: { string: boolean },
  intersection_data: Object
): { string: number } {
  let access_score_profile = {} as { string: number };

  for (let key of Object.keys(modes_transport)) {
    if (modes_transport[key]) {
      access_score_profile[key] = calculateAccessScoreOnce(
        key,
        intersection_data
      );
    }
  }

  return access_score_profile;
}

// Calculates the access score for a single mode of transport at a given intersection
function calculateAccessScoreOnce(
  transport: string,
  intersection_data: Object
): number {
  const re = new RegExp(`^${transport}_\\w*$`);
  let scores = [];
  for (let key of Object.keys(intersection_data)) {
    if (key.match(re)) {
      scores.push(intersection_data[key]);
    }
  }

  let avg = scores.reduce((a, b) => a + b, 0) / scores.length;
  return avg;
}

// Aggregates the access scores of all intersections from a user
export function mergeAccessScores(access_scores: { string: number }[]): {
  string: number;
} {
  let aggregate_score = {} as { string: number };

  // TODO: Define these labels in a centralized location
  for (let mode of ["pedestrian", "bicycle", "bus", "vehicle"]) {
    const scores = access_scores
      .map((access_score) => access_score[mode])
      .filter(Boolean);

    const avg = scores.reduce((a, b) => a + b, 0) / scores.length;

    aggregate_score[mode] = avg;
  }

  return aggregate_score;
}
