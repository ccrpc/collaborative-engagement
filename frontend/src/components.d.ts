/* eslint-disable */
/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */


import { HTMLStencilElement, JSXBase } from '@stencil/core/internal';
import {
  ITrip,
  IUser,
} from '@ccrpc/cet-shared-lib';
import {
  IIntersectionFeature,
  MapBoxGLFeature,
} from './interfaces';
import {
  tripInfo,
} from './components/ce-root/ce-root';

export namespace Components {
  interface CeAccessScore {
    'latitude': number;
    'longitude': number;
    'maxzoom': number;
    'minzoom': number;
    'zoom': number;
  }
  interface CeAccessScoreIntro {
    'current_activity_index': number;
  }
  interface CeAccessScoreTable {
    'url': string;
  }
  interface CeComment {
    '_id': number;
    'dateModified': Date;
    'datePublished': Date;
    'poster': IUser;
    'text': string;
    'trip': ITrip;
  }
  interface CeCommentBoard {}
  interface CeDestination {
    /**
    * An identifier for this browser, used by the database to identify Users. Stored as a uuid version 4 entity.
    */
    'device_id': string;
    /**
    * The intersection closes to the user's home
    */
    'home_intersection': IIntersectionFeature;
    /**
    * The latitude of the center of the map
    */
    'latitude': number;
    /**
    * The longitude of the center of the map
    */
    'longitude': number;
    /**
    * The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
    */
    'maxzoom': number;
    /**
    * The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed
    */
    'minzoom': number;
    /**
    * A cache of already submitted trips
    */
    'tripInfoCache': tripInfo[];
    /**
    * The zoom level of the center of the map
    */
    'zoom': number;
  }
  interface CeDestinationForm {
    /**
    * The features the user clicked on to generate this popup.  Passed along via webmapgl.
    */
    'features': MapBoxGLFeature<any>[];
    /**
    * A cache of already saved trips
    */
    'tripInfoCache': tripInfo[];
  }
  interface CeDestinationIntro {
    'current_activity_index': number;
  }
  interface CeMap {
    /**
    * The latitude of the center of the map
    */
    'latitude': number;
    /**
    * The longitude of the center of the map
    */
    'longitude': number;
    /**
    * What type of map to render
    */
    'mapType': "pretest" | "pretest-view" | "access-score" | "destination";
    /**
    * The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
    */
    'maxzoom': number;
    /**
    * The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed
    */
    'minzoom': number;
    /**
    * The intersection to be highlighted
    */
    'selected_intersection': IIntersectionFeature;
    /**
    * A cache of the already saved trips
    */
    'tripInfoCache': tripInfo[];
    /**
    * The zoom level of the center of the map
    */
    'zoom': number;
  }
  interface CePretest {
    /**
    * The index of the current activity, used to refresh the page should the user invalidate their home
    */
    'current_activity_index': number;
    /**
    * The current home intersection of the user, if there is one.  At the start of the ce-pretest activity it is assumed there is not.
    */
    'home_intersection': IIntersectionFeature;
    /**
    * The latitude of the center of the map
    */
    'latitude': number;
    /**
    * The longitude of the center of the map
    */
    'longitude': number;
    /**
    * The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
    */
    'maxzoom': number;
    /**
    * The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed
    */
    'minzoom': number;
    /**
    * The zoom level of the center of the map
    */
    'zoom': number;
  }
  interface CePretestForm {
    /**
    * The feature(s) which spawned this popup, passed in from webmapgl
    */
    'features': IIntersectionFeature[];
    /**
    * Whether the user has already selected a home intersection.
    */
    'haveSelected': boolean;
  }
  interface CePretestView {
    /**
    * The index of the current activity, used to refresh the page should the user invalidate their home
    */
    'current_activity_index': number;
    /**
    * The intersection closes to the user's home
    */
    'home_intersection': IIntersectionFeature;
    /**
    * The latitude of the center of the map
    */
    'latitude': number;
    /**
    * The longitude of the center of the map
    */
    'longitude': number;
    /**
    * The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
    */
    'maxzoom': number;
    /**
    * The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed
    */
    'minzoom': number;
    /**
    * The zoom level of the center of the map
    */
    'zoom': number;
  }
  interface CeRoot {
    /**
    * The latitude of the center of the map
    */
    'latitude': number;
    /**
    * The longitude of the center of the map
    */
    'longitude': number;
    /**
    * The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
    */
    'maxzoom': number;
    /**
    * The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed
    */
    'minzoom': number;
    /**
    * The zoom level of the center of the map
    */
    'zoom': number;
  }
  interface CeSubmission {
    /**
    * A cache of already submitted trips
    */
    'tripInfoCache': tripInfo[];
    'username': string;
  }
  interface CeWelcome {
    'current_activity_index': number;
  }
  interface CeWrapUp {}
}

declare global {


  interface HTMLCeAccessScoreElement extends Components.CeAccessScore, HTMLStencilElement {}
  var HTMLCeAccessScoreElement: {
    prototype: HTMLCeAccessScoreElement;
    new (): HTMLCeAccessScoreElement;
  };

  interface HTMLCeAccessScoreIntroElement extends Components.CeAccessScoreIntro, HTMLStencilElement {}
  var HTMLCeAccessScoreIntroElement: {
    prototype: HTMLCeAccessScoreIntroElement;
    new (): HTMLCeAccessScoreIntroElement;
  };

  interface HTMLCeAccessScoreTableElement extends Components.CeAccessScoreTable, HTMLStencilElement {}
  var HTMLCeAccessScoreTableElement: {
    prototype: HTMLCeAccessScoreTableElement;
    new (): HTMLCeAccessScoreTableElement;
  };

  interface HTMLCeCommentElement extends Components.CeComment, HTMLStencilElement {}
  var HTMLCeCommentElement: {
    prototype: HTMLCeCommentElement;
    new (): HTMLCeCommentElement;
  };

  interface HTMLCeCommentBoardElement extends Components.CeCommentBoard, HTMLStencilElement {}
  var HTMLCeCommentBoardElement: {
    prototype: HTMLCeCommentBoardElement;
    new (): HTMLCeCommentBoardElement;
  };

  interface HTMLCeDestinationElement extends Components.CeDestination, HTMLStencilElement {}
  var HTMLCeDestinationElement: {
    prototype: HTMLCeDestinationElement;
    new (): HTMLCeDestinationElement;
  };

  interface HTMLCeDestinationFormElement extends Components.CeDestinationForm, HTMLStencilElement {}
  var HTMLCeDestinationFormElement: {
    prototype: HTMLCeDestinationFormElement;
    new (): HTMLCeDestinationFormElement;
  };

  interface HTMLCeDestinationIntroElement extends Components.CeDestinationIntro, HTMLStencilElement {}
  var HTMLCeDestinationIntroElement: {
    prototype: HTMLCeDestinationIntroElement;
    new (): HTMLCeDestinationIntroElement;
  };

  interface HTMLCeMapElement extends Components.CeMap, HTMLStencilElement {}
  var HTMLCeMapElement: {
    prototype: HTMLCeMapElement;
    new (): HTMLCeMapElement;
  };

  interface HTMLCePretestElement extends Components.CePretest, HTMLStencilElement {}
  var HTMLCePretestElement: {
    prototype: HTMLCePretestElement;
    new (): HTMLCePretestElement;
  };

  interface HTMLCePretestFormElement extends Components.CePretestForm, HTMLStencilElement {}
  var HTMLCePretestFormElement: {
    prototype: HTMLCePretestFormElement;
    new (): HTMLCePretestFormElement;
  };

  interface HTMLCePretestViewElement extends Components.CePretestView, HTMLStencilElement {}
  var HTMLCePretestViewElement: {
    prototype: HTMLCePretestViewElement;
    new (): HTMLCePretestViewElement;
  };

  interface HTMLCeRootElement extends Components.CeRoot, HTMLStencilElement {}
  var HTMLCeRootElement: {
    prototype: HTMLCeRootElement;
    new (): HTMLCeRootElement;
  };

  interface HTMLCeSubmissionElement extends Components.CeSubmission, HTMLStencilElement {}
  var HTMLCeSubmissionElement: {
    prototype: HTMLCeSubmissionElement;
    new (): HTMLCeSubmissionElement;
  };

  interface HTMLCeWelcomeElement extends Components.CeWelcome, HTMLStencilElement {}
  var HTMLCeWelcomeElement: {
    prototype: HTMLCeWelcomeElement;
    new (): HTMLCeWelcomeElement;
  };

  interface HTMLCeWrapUpElement extends Components.CeWrapUp, HTMLStencilElement {}
  var HTMLCeWrapUpElement: {
    prototype: HTMLCeWrapUpElement;
    new (): HTMLCeWrapUpElement;
  };
  interface HTMLElementTagNameMap {
    'ce-access-score': HTMLCeAccessScoreElement;
    'ce-access-score-intro': HTMLCeAccessScoreIntroElement;
    'ce-access-score-table': HTMLCeAccessScoreTableElement;
    'ce-comment': HTMLCeCommentElement;
    'ce-comment-board': HTMLCeCommentBoardElement;
    'ce-destination': HTMLCeDestinationElement;
    'ce-destination-form': HTMLCeDestinationFormElement;
    'ce-destination-intro': HTMLCeDestinationIntroElement;
    'ce-map': HTMLCeMapElement;
    'ce-pretest': HTMLCePretestElement;
    'ce-pretest-form': HTMLCePretestFormElement;
    'ce-pretest-view': HTMLCePretestViewElement;
    'ce-root': HTMLCeRootElement;
    'ce-submission': HTMLCeSubmissionElement;
    'ce-welcome': HTMLCeWelcomeElement;
    'ce-wrap-up': HTMLCeWrapUpElement;
  }
}

declare namespace LocalJSX {
  interface CeAccessScore {
    'latitude'?: number;
    'longitude'?: number;
    'maxzoom'?: number;
    'minzoom'?: number;
    'zoom'?: number;
  }
  interface CeAccessScoreIntro {
    'current_activity_index'?: number;
  }
  interface CeAccessScoreTable {
    'url'?: string;
  }
  interface CeComment {
    '_id'?: number;
    'dateModified'?: Date;
    'datePublished'?: Date;
    'poster'?: IUser;
    'text'?: string;
    'trip'?: ITrip;
  }
  interface CeCommentBoard {}
  interface CeDestination {
    /**
    * An identifier for this browser, used by the database to identify Users. Stored as a uuid version 4 entity.
    */
    'device_id'?: string;
    /**
    * The intersection closes to the user's home
    */
    'home_intersection'?: IIntersectionFeature;
    /**
    * The latitude of the center of the map
    */
    'latitude'?: number;
    /**
    * The longitude of the center of the map
    */
    'longitude'?: number;
    /**
    * The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
    */
    'maxzoom'?: number;
    /**
    * The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed
    */
    'minzoom'?: number;
    /**
    * Event that trips have been submitted
    */
    'onTripSubmit'?: (event: CustomEvent<ITrip>) => void;
    /**
    * A cache of already submitted trips
    */
    'tripInfoCache'?: tripInfo[];
    /**
    * The zoom level of the center of the map
    */
    'zoom'?: number;
  }
  interface CeDestinationForm {
    /**
    * The features the user clicked on to generate this popup.  Passed along via webmapgl.
    */
    'features'?: MapBoxGLFeature<any>[];
    /**
    * An event emitted when the user has entered and submitted information about the destination this popup was created for
    */
    'onDestinationFormSubmitted'?: (event: CustomEvent<ITrip>) => void;
    /**
    * A cache of already saved trips
    */
    'tripInfoCache'?: tripInfo[];
  }
  interface CeDestinationIntro {
    'current_activity_index'?: number;
  }
  interface CeMap {
    /**
    * The latitude of the center of the map
    */
    'latitude'?: number;
    /**
    * The longitude of the center of the map
    */
    'longitude'?: number;
    /**
    * What type of map to render
    */
    'mapType'?: "pretest" | "pretest-view" | "access-score" | "destination";
    /**
    * The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
    */
    'maxzoom'?: number;
    /**
    * The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed
    */
    'minzoom'?: number;
    /**
    * The intersection to be highlighted
    */
    'selected_intersection'?: IIntersectionFeature;
    /**
    * A cache of the already saved trips
    */
    'tripInfoCache'?: tripInfo[];
    /**
    * The zoom level of the center of the map
    */
    'zoom'?: number;
  }
  interface CePretest {
    /**
    * The index of the current activity, used to refresh the page should the user invalidate their home
    */
    'current_activity_index'?: number;
    /**
    * The current home intersection of the user, if there is one.  At the start of the ce-pretest activity it is assumed there is not.
    */
    'home_intersection'?: IIntersectionFeature;
    /**
    * The latitude of the center of the map
    */
    'latitude'?: number;
    /**
    * The longitude of the center of the map
    */
    'longitude'?: number;
    /**
    * The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
    */
    'maxzoom'?: number;
    /**
    * The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed
    */
    'minzoom'?: number;
    /**
    * Event to change the page once an intersection has been selected
    */
    'onPageChanged'?: (event: CustomEvent<any>) => void;
    /**
    * Event to report to the parent that the user has selected an intersection (and what that intersection is)
    */
    'onPretestSubmitted'?: (event: CustomEvent<any>) => void;
    /**
    * The zoom level of the center of the map
    */
    'zoom'?: number;
  }
  interface CePretestForm {
    /**
    * The feature(s) which spawned this popup, passed in from webmapgl
    */
    'features'?: IIntersectionFeature[];
    /**
    * Whether the user has already selected a home intersection.
    */
    'haveSelected'?: boolean;
    /**
    * An event to emit when the user has selected an intersection.
    */
    'onIntersectionSelected'?: (event: CustomEvent<boolean>) => void;
  }
  interface CePretestView {
    /**
    * The index of the current activity, used to refresh the page should the user invalidate their home
    */
    'current_activity_index'?: number;
    /**
    * The intersection closes to the user's home
    */
    'home_intersection'?: IIntersectionFeature;
    /**
    * The latitude of the center of the map
    */
    'latitude'?: number;
    /**
    * The longitude of the center of the map
    */
    'longitude'?: number;
    /**
    * The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
    */
    'maxzoom'?: number;
    /**
    * The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed
    */
    'minzoom'?: number;
    /**
    * Used to change / refresh the page should the user invalidate their home
    */
    'onPageChanged'?: (event: CustomEvent<number>) => void;
    /**
    * Communicates to the root that the user has decided to invalidate their home
    */
    'onPretestSubmitted'?: (event: CustomEvent<IIntersectionFeature>) => void;
    /**
    * The zoom level of the center of the map
    */
    'zoom'?: number;
  }
  interface CeRoot {
    /**
    * The latitude of the center of the map
    */
    'latitude'?: number;
    /**
    * The longitude of the center of the map
    */
    'longitude'?: number;
    /**
    * The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
    */
    'maxzoom'?: number;
    /**
    * The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed
    */
    'minzoom'?: number;
    /**
    * An event that signifies the map should toggle the visibility of the glAddressSearch bar Note that having a parent component emit an event that is listened to by a child component goes against the stencil idiom which would have such data passed as a property. That said using a property causes the map to flash which is not desired.  The current theory as to why the map flashes is that it is created in a child component and therefore redrawn when any of said child component's Props changes. Other applications that have the map in the root component, such as SSET (gitlab.com/ccrpc/sset/) or LUI (https://gitlab.com/ccrpc/land-use-inventory/), do not have this problem.
    */
    'onAddressSearchToggle'?: (event: CustomEvent<void>) => void;
    /**
    * Emitted when we want to add to a source in the current map
    */
    'onMapSourceAdd'?: (event: CustomEvent<ITrip>) => void;
    /**
    * An event that is triggered when the page is changed
    */
    'onPageChanged'?: (event: CustomEvent<number>) => void;
    /**
    * The zoom level of the center of the map
    */
    'zoom'?: number;
  }
  interface CeSubmission {
    'onTripsSubmitted'?: (event: CustomEvent<void>) => void;
    'onUsernameChange'?: (event: CustomEvent<string>) => void;
    /**
    * A cache of already submitted trips
    */
    'tripInfoCache'?: tripInfo[];
    'username'?: string;
  }
  interface CeWelcome {
    'current_activity_index'?: number;
  }
  interface CeWrapUp {}

  interface IntrinsicElements {
    'ce-access-score': CeAccessScore;
    'ce-access-score-intro': CeAccessScoreIntro;
    'ce-access-score-table': CeAccessScoreTable;
    'ce-comment': CeComment;
    'ce-comment-board': CeCommentBoard;
    'ce-destination': CeDestination;
    'ce-destination-form': CeDestinationForm;
    'ce-destination-intro': CeDestinationIntro;
    'ce-map': CeMap;
    'ce-pretest': CePretest;
    'ce-pretest-form': CePretestForm;
    'ce-pretest-view': CePretestView;
    'ce-root': CeRoot;
    'ce-submission': CeSubmission;
    'ce-welcome': CeWelcome;
    'ce-wrap-up': CeWrapUp;
  }
}

export { LocalJSX as JSX };


declare module "@stencil/core" {
  export namespace JSX {
    interface IntrinsicElements {
      'ce-access-score': LocalJSX.CeAccessScore & JSXBase.HTMLAttributes<HTMLCeAccessScoreElement>;
      'ce-access-score-intro': LocalJSX.CeAccessScoreIntro & JSXBase.HTMLAttributes<HTMLCeAccessScoreIntroElement>;
      'ce-access-score-table': LocalJSX.CeAccessScoreTable & JSXBase.HTMLAttributes<HTMLCeAccessScoreTableElement>;
      'ce-comment': LocalJSX.CeComment & JSXBase.HTMLAttributes<HTMLCeCommentElement>;
      'ce-comment-board': LocalJSX.CeCommentBoard & JSXBase.HTMLAttributes<HTMLCeCommentBoardElement>;
      'ce-destination': LocalJSX.CeDestination & JSXBase.HTMLAttributes<HTMLCeDestinationElement>;
      'ce-destination-form': LocalJSX.CeDestinationForm & JSXBase.HTMLAttributes<HTMLCeDestinationFormElement>;
      'ce-destination-intro': LocalJSX.CeDestinationIntro & JSXBase.HTMLAttributes<HTMLCeDestinationIntroElement>;
      'ce-map': LocalJSX.CeMap & JSXBase.HTMLAttributes<HTMLCeMapElement>;
      'ce-pretest': LocalJSX.CePretest & JSXBase.HTMLAttributes<HTMLCePretestElement>;
      'ce-pretest-form': LocalJSX.CePretestForm & JSXBase.HTMLAttributes<HTMLCePretestFormElement>;
      'ce-pretest-view': LocalJSX.CePretestView & JSXBase.HTMLAttributes<HTMLCePretestViewElement>;
      'ce-root': LocalJSX.CeRoot & JSXBase.HTMLAttributes<HTMLCeRootElement>;
      'ce-submission': LocalJSX.CeSubmission & JSXBase.HTMLAttributes<HTMLCeSubmissionElement>;
      'ce-welcome': LocalJSX.CeWelcome & JSXBase.HTMLAttributes<HTMLCeWelcomeElement>;
      'ce-wrap-up': LocalJSX.CeWrapUp & JSXBase.HTMLAttributes<HTMLCeWrapUpElement>;
    }
  }
}


