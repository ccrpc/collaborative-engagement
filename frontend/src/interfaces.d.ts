import { IIntersection, IDestination } from "@ccrpc/cet-shared-lib";

export interface IActivity {
  readonly activity_title: string;
  readonly activity_prompt?: string;
}

export interface IMapApplication {
  latitude: number;
  longitude: number;
  zoom: number;
  minzoom: number;
  maxzoom: number;
}

export interface IMapActivity extends IActivity {
  latitude: number;
  longitude: number;
  zoom: number;
  minzoom: number;
  maxzoom: number;
}

export interface IPage {
  readonly page_title: string;
}

interface GeoJsonGeometry {
  type:
    | "Point"
    | "MultiPoint"
    | "LineString"
    | "MultiLineString"
    | "Polygon"
    | "MultiPolygon"
    | "GeometryCollection";
  // If it is point the positions are one array with longitude and latitude, in that order. See also: https://www.rfc-editor.org/rfc/rfc7946#section-3.1
  coordinates?: any[];
}

export interface MapBoxGLFeature<Properties> {
  _geometry?: GeoJsonGeometry;
  _vectorTileFeature?: {
    _geometry: number;
    _keys: string[];
    _pbf: Object;
    _values: number[];
    _x: number;
    _y: number;
    _z: number;
    extent: number;
    properties: Properties;
  };
  geometry: GeoJsonGeometry;
  id: string | number;
  layer: {
    id: string;
    maxzoom: number;
    minzoom: number;
    paint: Object;
    source: string;
    "source-layer": string;
    type: string;
  };
  properties: Properties;
  source: string;
  sourceLayer: string;
  state: Object;
  type: "Feature";
}

export interface IIntersectionFeature extends MapBoxGLFeature<IIntersection> {
  _geometry?: {
    type: "Point";
    coordinates: [number, number];
  };
  geometry: {
    type: "Point";
    coordinates: [number, number];
  };
}

export interface IDestinationFeature extends MapBoxGLFeature<IDestination> {
  _geometry?: {
    type: "Point";
    coordinates: [number, number];
  };
  geometry: {
    type: "Point";
    coordinates: [number, number];
  };
}
