export const webMapSources = {
  "access-score": {
    "access-score": {
      url: "https://maps.ccrpc.org/collaborative-engagement/pois_access.json",
      type: "vector",
    },
  },
  trips: {
    trips: {
      type: "geojson",
      data: {
        type: "FeatureCollection",
        features: [],
      },
    },
  },
};
