# ce-destination



<!-- Auto Generated Below -->


## Properties

| Property            | Attribute   | Description                                                                                                                                                                        | Type                   | Default     |
| ------------------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- | ----------- |
| `device_id`         | `device_id` | An identifier for this browser, used by the database to identify Users. Stored as a uuid version 4 entity.                                                                         | `string`               | `undefined` |
| `home_intersection` | --          | The intersection closes to the user's home                                                                                                                                         | `IIntersectionFeature` | `undefined` |
| `latitude`          | `latitude`  | The latitude of the center of the map                                                                                                                                              | `number`               | `undefined` |
| `longitude`         | `longitude` | The longitude of the center of the map                                                                                                                                             | `number`               | `undefined` |
| `maxzoom`           | `maxzoom`   | The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it | `number`               | `undefined` |
| `minzoom`           | `minzoom`   | The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed            | `number`               | `undefined` |
| `tripInfoCache`     | --          | A cache of already submitted trips                                                                                                                                                 | `tripInfo[]`           | `undefined` |
| `zoom`              | `zoom`      | The zoom level of the center of the map                                                                                                                                            | `number`               | `undefined` |


## Events

| Event        | Description                          | Type                 |
| ------------ | ------------------------------------ | -------------------- |
| `tripSubmit` | Event that trips have been submitted | `CustomEvent<ITrip>` |


## Dependencies

### Depends on

- [ce-map](../ce-map)
- ion-thumbnail

### Graph
```mermaid
graph TD;
  ce-destination --> ce-map
  ce-destination --> ion-thumbnail
  ce-map --> gl-drawer-toggle
  ce-map --> gl-drawer
  ce-map --> ce-access-score-table
  ce-map --> gl-map
  ce-map --> gl-address-search
  ce-map --> gl-style
  ce-map --> gl-popup
  gl-drawer-toggle --> ion-button
  gl-drawer-toggle --> ion-icon
  gl-drawer-toggle --> gl-drawer
  ion-button --> ion-ripple-effect
  gl-drawer --> ion-header
  gl-drawer --> ion-toolbar
  gl-drawer --> ion-buttons
  gl-drawer --> ion-title
  gl-drawer --> ion-button
  gl-drawer --> ion-icon
  gl-drawer --> ion-content
  ce-access-score-table --> ion-icon
  gl-address-search --> ion-item
  gl-address-search --> ion-icon
  gl-address-search --> ion-label
  gl-address-search --> ion-list
  gl-address-search --> ion-searchbar
  gl-address-search --> gl-geocode-controller
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-searchbar --> ion-icon
  style ce-destination fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
