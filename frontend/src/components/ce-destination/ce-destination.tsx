import { Component, Prop, Listen, h, Event, EventEmitter } from "@stencil/core";
import { ITrip } from "@ccrpc/cet-shared-lib";
import { IDestinationFeature, IIntersectionFeature } from "../../interfaces";
import { getMap } from "../../utils";
import { webMapSources } from "../../assets/styles/sources";
import { tripInfo } from "../ce-root/ce-root";
declare const mapboxgl;

/** In this activity the user clicks on a destination which creates a popup in which they can report the ease of getting to that destination
 * via any of the four transportation modes.
 *
 * This can be done more than once without having to leave this activity.
 */
@Component({
  tag: "ce-destination",
  styleUrl: "ce-destination.css",
  // shadow: true
})
export class Destination {
  /** Event that trips have been submitted */
  @Event() tripSubmit: EventEmitter<ITrip>;

  /** The latitude of the center of the map */
  @Prop() latitude: number;

  /** The longitude of the center of the map */
  @Prop() longitude: number;

  /** The zoom level of the center of the map */
  @Prop() zoom: number;

  /** The minimum zoom of the map, user will not be able to zoom out more than this
   * If a tile is needed at a zoom below this point it will neither be requested nor displayed
   */
  @Prop() minzoom: number;

  /** The maximum zoom of the map
   * If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
   */
  @Prop() maxzoom: number;

  /** The intersection closes to the user's home */
  @Prop() home_intersection: IIntersectionFeature = undefined;

  /** An identifier for this browser, used by the database to identify Users.
   * Stored as a uuid version 4 entity.
   */
  @Prop() device_id: string;

  /** A cache of already submitted trips */
  @Prop() tripInfoCache: tripInfo[];

  /** A reference to the webmapgl map element this component displays */
  map: HTMLGlMapElement;

  /** Whether hover has been enabled */
  hoverEnabled: boolean = false;
  /** A list of the layer names hover should be enabled for */
  hoverLayer: string = "intersections:destination-click";
  /** A mapboxgl Popup used to display information about destinations on hover */
  hoverPopup: any = undefined;

  /** References to the hover handlers so they can be added and removed */
  handlers = {
    mouseenter: (e: any) => {
      this.hoverEvent(e);
    },
    mouseleave: () => {
      this.hoverPopup.remove();
    },
  };

  createPopup() {
    this.hoverPopup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
    });
    console.log("Hover popup created");
  }

  /** Enables or disables the popups hovering when the user moves the mouse over a destination
   * @param {boolean} enable - Whether to enable or disable hovering
   */
  setHover(enable: boolean) {
    if (this.map === undefined) {
      this.map = getMap();
    }

    if (enable) {
      this.addFunctions();
    } else {
      this.removeFunctions();
    }
    this.hoverEnabled = enable;
  }

  removeFunctions() {
    this.map = getMap();
    if (!this.map) return;

    console.log("Removing mouse enter function");
    this.map.map.off("mousemove", this.hoverLayer, this.handlers.mouseenter);

    console.log("Removing mouse leave function");
    this.map.map.off("mouseleave", this.hoverLayer, this.handlers.mouseleave);
  }

  addFunctions() {
    this.map = getMap();

    console.log("Attaching hover functions");

    this.map.map.on("mouseenter", this.hoverLayer, this.handlers.mouseenter);
    this.map.map.on("mouseleave", this.hoverLayer, this.handlers.mouseleave);
  }

  hoverEvent(event: any): void {
    if (this.map === undefined) {
      this.map = getMap();
    }

    if (!event.features) {
      console.log("Hover on layer without features");
      console.log(event);
      return;
    }

    // Copy coordinates array.
    // If there are multiple destinations at this point, their coordinates will be the same.
    const coordinates = event.features[0].geometry.coordinates.slice();

    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(event.lngLat.lng - coordinates[0]) > 180) {
      coordinates[0] += event.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    // Populate the popup and set its coordinates based on the feature found.
    this.hoverPopup
      .setLngLat(coordinates)
      .setHTML(this.hoverDescription(event.features))
      .addTo(this.map.map);
  }

  /** Returns an html string to be used in mapboxgl's setHTML */
  hoverDescription(features: IDestinationFeature[]): string {
    let htmlString = "<ion-item-group>";

    for (let i = 0; i < features.length; i++) {
      htmlString += `<ion-item lines=${
        i === features.length - 1 ? "none" : "full"
      }>
      <ion-label class="ion-text-wrap">
        ${features[i].properties.name} (${features[i].properties.type})
      </ion-label>
    </ion-item>`;
    }

    htmlString += "</ion-item-group>";

    return htmlString;
  }

  /** Ease of access to a given destination from an intersection are stored as Trips entities in the database.
   *
   * This function posts the trips data of the (up to) four movement types from the users home intersection to the destination the popup was open on to the database.
   *
   * The user also provides a comment about their scoring when the use the ce-destination-form popup, this is also sent.
   */
  postData(trip: ITrip) {
    console.log("Reporting trips to root");

    trip.user = {
      _id: undefined,
      username: undefined,
      device_id: this.device_id,
      comments: undefined,
      trips: undefined,
    };

    trip.origin = {
      ...this.home_intersection.properties,
    };

    this.tripSubmit.emit(trip);
  }

  componentWillLoad() {
    this.hoverEnabled = false;
  }

  render() {
    console.log("Destination Page Rendering");
    return (
      <ce-map
        mapType="destination"
        longitude={this.longitude}
        latitude={this.latitude}
        zoom={this.zoom}
        minzoom={this.minzoom}
        maxzoom={this.maxzoom}
        selected_intersection={this.home_intersection}
        tripInfoCache={this.tripInfoCache}
      ></ce-map>
    );
  }

  componentDidRender() {
    console.log(this.tripInfoCache);
    if (this.hoverPopup === undefined) {
      console.log("Creating hover popup");
      this.createPopup();
    }
    if (!this.hoverEnabled) {
      console.log("Enabling hover functionality");
      this.setHover(true);
    }

    console.log("Destination Page has rendered");
  }

  disconnectedCallback() {
    if (this.hoverEnabled) {
      console.log("Removing hover functions");
      this.setHover(false);
    }
    if (this.map !== undefined) {
      this.map = undefined;
    }
  }

  /** The user has selected and rated a destination
   *
   * Notably here is where the "origin_id" is added to the trips as the popup does not have that information
   */
  @Listen("destinationFormSubmitted", { target: "body" })
  handleTrips(e: CustomEvent<ITrip>) {
    let tripsRequest = e["detail"];
    if (tripsRequest.scores.length === 0) {
      alert("Please select at least one transportation method");
      return;
    }

    // Save trip to cache to be posted later
    this.postData(tripsRequest);

    // close the ce-destination-form popup
    const popup = document.querySelector(".mapboxgl-popup");
    popup.remove();
  }

  @Listen("mapSourceAdd", { target: "body" })
  addToSource(event: CustomEvent<ITrip>) {
    const trip = event.detail ? event.detail : undefined;
    if (!trip) {
      console.log("Destination received empty trip request to add to source");
    } else {
      console.log(
        `Destination received trip request [${trip.destination.name}] to add to source`
      );
    }
    const map = getMap();
    webMapSources.trips.trips.data.features.push({
      type: "Feature",
      geometry: {
        type: "LineString",
        coordinates: [
          this.home_intersection.geometry.coordinates,
          trip.destination.geom.coordinates,
        ],
      },
      properties: {
        name: trip.destination.name,
      },
    });
    map.map
      .getSource("intersections:trips")
      .setData(webMapSources.trips.trips.data);
  }
}
