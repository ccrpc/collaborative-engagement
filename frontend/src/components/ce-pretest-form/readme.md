# ce-pretest-form



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                                      | Type                     | Default     |
| -------------- | --------------- | ---------------------------------------------------------------- | ------------------------ | ----------- |
| `features`     | --              | The feature(s) which spawned this popup, passed in from webmapgl | `IIntersectionFeature[]` | `undefined` |
| `haveSelected` | `have-selected` | Whether the user has already selected a home intersection.       | `boolean`                | `false`     |


## Events

| Event                  | Description                                                  | Type                   |
| ---------------------- | ------------------------------------------------------------ | ---------------------- |
| `intersectionSelected` | An event to emit when the user has selected an intersection. | `CustomEvent<boolean>` |


## Dependencies

### Used by

 - [ce-pretest-view](../ce-pretest-view)

### Depends on

- ion-item-group
- ion-item
- ion-label
- ion-button

### Graph
```mermaid
graph TD;
  ce-pretest-form --> ion-item-group
  ce-pretest-form --> ion-item
  ce-pretest-form --> ion-label
  ce-pretest-form --> ion-button
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-button --> ion-ripple-effect
  ce-pretest-view --> ce-pretest-form
  style ce-pretest-form fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
