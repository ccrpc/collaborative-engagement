import { Component, Event, EventEmitter, h, Prop } from "@stencil/core";
import { IIntersectionFeature } from "../../interfaces";

/** This is the contents of the popup a user will see during the ce-pretest activity prompting them to select the intersections closest to their home */
@Component({
  tag: "ce-pretest-form",
  styleUrl: "pretest-form.css",
  // shadow: true
})
export class PretestForm {
  /** An event to emit when the user has selected an intersection. */
  @Event() intersectionSelected: EventEmitter<boolean>;

  /** The feature(s) which spawned this popup, passed in from webmapgl */
  @Prop() features: IIntersectionFeature[];

  /** Whether the user has already selected a home intersection.
   */
  @Prop() haveSelected: boolean = false;

  render() {
    return (
      <form id="pretestForm" method="" onSubmit={() => false}>
        <ion-item-group>
          <ion-item>
            <ion-label>
              <h2>Select the intersection closest to your home</h2>
            </ion-label>
          </ion-item>
        </ion-item-group>
        <ion-button
          expand="block"
          onClick={() => this.intersectionSelected.emit(!this.haveSelected)}
        >
          {this.haveSelected
            ? "Select a new home"
            : "Select this intersection as your home"}
        </ion-button>
      </form>
    );
  }
}
