# ce-access-score



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute   | Description | Type     | Default     |
| ----------- | ----------- | ----------- | -------- | ----------- |
| `latitude`  | `latitude`  |             | `number` | `undefined` |
| `longitude` | `longitude` |             | `number` | `undefined` |
| `maxzoom`   | `maxzoom`   |             | `number` | `undefined` |
| `minzoom`   | `minzoom`   |             | `number` | `undefined` |
| `zoom`      | `zoom`      |             | `number` | `undefined` |


## Dependencies

### Depends on

- [ce-map](../ce-map)

### Graph
```mermaid
graph TD;
  ce-access-score --> ce-map
  ce-map --> gl-drawer-toggle
  ce-map --> gl-drawer
  ce-map --> ce-access-score-table
  ce-map --> gl-map
  ce-map --> gl-address-search
  ce-map --> gl-style
  ce-map --> gl-popup
  gl-drawer-toggle --> ion-button
  gl-drawer-toggle --> ion-icon
  gl-drawer-toggle --> gl-drawer
  ion-button --> ion-ripple-effect
  gl-drawer --> ion-header
  gl-drawer --> ion-toolbar
  gl-drawer --> ion-buttons
  gl-drawer --> ion-title
  gl-drawer --> ion-button
  gl-drawer --> ion-icon
  gl-drawer --> ion-content
  ce-access-score-table --> ion-icon
  gl-address-search --> ion-item
  gl-address-search --> ion-icon
  gl-address-search --> ion-label
  gl-address-search --> ion-list
  gl-address-search --> ion-searchbar
  gl-address-search --> gl-geocode-controller
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-searchbar --> ion-icon
  style ce-access-score fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
