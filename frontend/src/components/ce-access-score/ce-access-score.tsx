import { Component, Prop, Listen, h } from "@stencil/core";

/** Currently unused */
@Component({
  tag: "ce-access-score",
  styleUrl: "ce-access-score.css",
})
export class AccessScore {
  // TODO: Collect Form Data here
  private _data: Object[] = [];
  // TODO: Store Viewable Info here
  private _popup_intersection_id = 1234;

  @Prop() latitude: number;
  @Prop() longitude: number;
  @Prop() zoom: number;
  @Prop() minzoom: number;
  @Prop() maxzoom: number;

  componentDidLoad() {
    console.log("Access Score page finished rendering");
  }

  render() {
    console.log("Access Score page rendering");
    return (
      <ce-map
        mapType="access-score"
        longitude={this.longitude}
        latitude={this.latitude}
        zoom={this.zoom}
        minzoom={this.minzoom}
        maxzoom={this.maxzoom}
      ></ce-map>
    );
  }

  @Listen("formSubmitted", { target: "body" })
  appendFormData(e: CustomEvent) {
    // Get data from the form
    let form_data = e["detail"];

    // Append form data to this component
    form_data["intersection_id"] = this._popup_intersection_id;
    this._data.push(form_data);

    // Close popup
    const popup = document.querySelector(".mapboxgl-popup");
    popup.remove();
  }
}
