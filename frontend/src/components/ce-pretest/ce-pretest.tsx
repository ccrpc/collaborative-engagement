import {
  Component,
  Listen,
  Prop,
  Event,
  EventEmitter,
  h,
  State,
} from "@stencil/core";
import { IIntersectionFeature, MapBoxGLFeature } from "../../interfaces";

@Component({
  tag: "ce-pretest",
  styleUrl: "ce-pretest.css",
  // shadow: true
})
export class Pretest {
  /** The latitude of the center of the map */
  @Prop() latitude: number;

  /** The longitude of the center of the map */
  @Prop() longitude: number;

  /** The zoom level of the center of the map */
  @Prop() zoom: number;

  /** The minimum zoom of the map, user will not be able to zoom out more than this
   * If a tile is needed at a zoom below this point it will neither be requested nor displayed
   */
  @Prop() minzoom: number;

  /** The maximum zoom of the map
   * If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
   */
  @Prop() maxzoom: number;

  /** The index of the current activity, used to refresh the page should the user invalidate their home
   */
  @Prop() current_activity_index: number;

  /** The current home intersection of the user, if there is one.
   *
   * At the start of the ce-pretest activity it is assumed there is not.
   */
  @Prop() home_intersection: IIntersectionFeature;

  /** The intersection the user has most recently clicked, this is different from home intersection as
   * the home intersection will be set to the selected one by an event that is pushed upwards.
   */
  @State() selectedIntersection: IIntersectionFeature;

  /** Event to change the page once an intersection has been selected */
  @Event() pageChanged: EventEmitter;

  /** Event to report to the parent that the user has selected an intersection (and what that intersection is) */
  @Event() pretestSubmitted: EventEmitter;

  /** Pre-populate the selected intersection if the user already selected an intersection as the closes to their house
   * and we are coming back to this component.
   */
  componentWillLoad() {
    if (this.home_intersection && !this.selectedIntersection) {
      this.selectedIntersection = this.home_intersection;
    }
  }

  render() {
    console.log("Pretest page rendering");
    return (
      <ce-map
        mapType="pretest"
        longitude={this.longitude}
        latitude={this.latitude}
        zoom={this.zoom}
        minzoom={this.minzoom}
        maxzoom={this.maxzoom}
        selected_intersection={this.home_intersection}
      ></ce-map>
    );
  }

  componentDidRender() {
    console.log("Pretest Page has rendered");
  }

  /**
   * The user has selected their home intersection
   * @param {CustomEvent<boolean>} e - The custom event passed up by stencil
   * @param {boolean} e.detail - Whether or not the user is selecting a home or invalidating it.
   */
  @Listen("intersectionSelected", { target: "body" })
  selectHome(e: CustomEvent<boolean>): void {
    const selectedHome: boolean = e.detail;

    // Close the popup
    const popup = document.querySelector(".mapboxgl-popup");
    popup.remove();

    this.pretestSubmitted.emit(
      selectedHome ? this.selectedIntersection : undefined
    );
    this.pageChanged.emit(this.current_activity_index + (selectedHome ? 1 : 0));
  }

  /** Listening to even emitted by webmapgl when the user clicks on feature(s) */
  @Listen("glClickedFeaturesGathered")
  handleFeatureClick(e: CustomEvent<{ features: MapBoxGLFeature<any>[] }>) {
    this.selectedIntersection = e.detail.features[0] as IIntersectionFeature;
  }
}
