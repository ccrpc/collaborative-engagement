# ce-pretest



<!-- Auto Generated Below -->


## Properties

| Property                 | Attribute                | Description                                                                                                                                                                        | Type                   | Default     |
| ------------------------ | ------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- | ----------- |
| `current_activity_index` | `current_activity_index` | The index of the current activity, used to refresh the page should the user invalidate their home                                                                                  | `number`               | `undefined` |
| `home_intersection`      | --                       | The current home intersection of the user, if there is one.  At the start of the ce-pretest activity it is assumed there is not.                                                   | `IIntersectionFeature` | `undefined` |
| `latitude`               | `latitude`               | The latitude of the center of the map                                                                                                                                              | `number`               | `undefined` |
| `longitude`              | `longitude`              | The longitude of the center of the map                                                                                                                                             | `number`               | `undefined` |
| `maxzoom`                | `maxzoom`                | The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it | `number`               | `undefined` |
| `minzoom`                | `minzoom`                | The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed            | `number`               | `undefined` |
| `zoom`                   | `zoom`                   | The zoom level of the center of the map                                                                                                                                            | `number`               | `undefined` |


## Events

| Event              | Description                                                                                              | Type               |
| ------------------ | -------------------------------------------------------------------------------------------------------- | ------------------ |
| `pageChanged`      | Event to change the page once an intersection has been selected                                          | `CustomEvent<any>` |
| `pretestSubmitted` | Event to report to the parent that the user has selected an intersection (and what that intersection is) | `CustomEvent<any>` |


## Dependencies

### Depends on

- [ce-map](../ce-map)

### Graph
```mermaid
graph TD;
  ce-pretest --> ce-map
  ce-map --> gl-drawer-toggle
  ce-map --> gl-drawer
  ce-map --> ce-access-score-table
  ce-map --> gl-map
  ce-map --> gl-address-search
  ce-map --> gl-style
  ce-map --> gl-popup
  gl-drawer-toggle --> ion-button
  gl-drawer-toggle --> ion-icon
  gl-drawer-toggle --> gl-drawer
  ion-button --> ion-ripple-effect
  gl-drawer --> ion-header
  gl-drawer --> ion-toolbar
  gl-drawer --> ion-buttons
  gl-drawer --> ion-title
  gl-drawer --> ion-button
  gl-drawer --> ion-icon
  gl-drawer --> ion-content
  ce-access-score-table --> ion-icon
  gl-address-search --> ion-item
  gl-address-search --> ion-icon
  gl-address-search --> ion-label
  gl-address-search --> ion-list
  gl-address-search --> ion-searchbar
  gl-address-search --> gl-geocode-controller
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-searchbar --> ion-icon
  style ce-pretest fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
