import { Component, h, Host } from "@stencil/core";

@Component({
  tag: "ce-wrap-up",
  styleUrl: "ce-wrap-up.css",
  shadow: true,
})
export class WrapUp {
  render() {
    return (
      <Host>
        <div>
          <h1>Thanks for participating!</h1>
        </div>
        <div class="video">
          <iframe
            src="https://www.youtube-nocookie.com/embed/FtlOrqUEzGA?modestbranding=1&origin=cet.ccrpc.org&cc_load_policy=1"
            title="CET Conclusion"
            frameborder="0"
            allowFullScreen={true}
            loading="lazy"
            seamless={true}
            class="iframe"
          ></iframe>
        </div>
        <div>
          <p>
            Thank you for your time; your input today will inform tomorrow's
            transportation system!
          </p>{" "}
          <p>
            For more information about the Access Score, visit{" "}
            <a
              href="https://ccrpc.gitlab.io/access-score/"
              target="_blank"
              rel="noreferrer noopener"
            >
              ccrpc.gitlab.io/access-score/
            </a>
            .
          </p>
          <p>
            {" "}
            If you have any questions or comments about this process or about
            transportation in the Champaign-Urbana region, email us anytime at
            cuuats@gmail.com or visit our website at{" "}
            <a
              href="https://ccrpc.org/programs/transportation/"
              target="_blank"
              rel="noreferrer noopener"
            >
              ccrpc.org/programs/transportation/
            </a>
          </p>
        </div>
      </Host>
    );
  }
}
