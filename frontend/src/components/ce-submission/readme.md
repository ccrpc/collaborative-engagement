# ce-submission



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute  | Description                        | Type         | Default     |
| --------------- | ---------- | ---------------------------------- | ------------ | ----------- |
| `tripInfoCache` | --         | A cache of already submitted trips | `tripInfo[]` | `undefined` |
| `username`      | `username` |                                    | `string`     | `undefined` |


## Events

| Event            | Description | Type                  |
| ---------------- | ----------- | --------------------- |
| `tripsSubmitted` |             | `CustomEvent<void>`   |
| `usernameChange` |             | `CustomEvent<string>` |


## Dependencies

### Depends on

- ion-col
- ion-label
- ion-icon
- ion-item
- ion-grid
- ion-row
- ion-note
- ion-textarea
- ion-list
- ion-button

### Graph
```mermaid
graph TD;
  ce-submission --> ion-col
  ce-submission --> ion-label
  ce-submission --> ion-icon
  ce-submission --> ion-item
  ce-submission --> ion-grid
  ce-submission --> ion-row
  ce-submission --> ion-note
  ce-submission --> ion-textarea
  ce-submission --> ion-list
  ce-submission --> ion-button
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-button --> ion-ripple-effect
  style ce-submission fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
