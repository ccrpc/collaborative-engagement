import { TextareaChangeEventDetail } from "@ionic/core";
import {
  Component,
  Prop,
  h,
  State,
  Host,
  EventEmitter,
  Event,
} from "@stencil/core";
import {
  IPostTripsRequest,
  ITrip,
  ModeTransportation,
} from "@ccrpc/cet-shared-lib";
import { tripInfo } from "../ce-root/ce-root";

const verbs: { [key in ModeTransportation]: string } = {
  [ModeTransportation.WALK]: "Walking",
  [ModeTransportation.BIKE]: "Biking",
  [ModeTransportation.BUS]: "Taking a bus",
  [ModeTransportation.DRIVE]: "Driving",
};

@Component({
  tag: "ce-submission",
  styleUrl: "ce-submission.css",
  shadow: true,
})
export class Submission {
  /** A cache of already submitted trips */
  @Prop() tripInfoCache: tripInfo[];

  @State() selectedTripIndex: number = 0;

  @Prop() username: string;

  @Event() usernameChange: EventEmitter<string>;

  @Event() tripsSubmitted: EventEmitter<void>;

  /** Update the user name */
  updateText(event: CustomEvent<TextareaChangeEventDetail>) {
    this.usernameChange.emit(event.detail.value);
  }

  createPostRequest(trip: ITrip): IPostTripsRequest {
    return {
      commentText: trip.comment.text,
      device_id: trip.user.device_id,
      username: this.username,
      origin_id: trip.origin.intersection_id,
      destinationName: trip.destination.name,
      destinationPoint: trip.destination.geom,
      destinationType: trip.destination.type,
      scores: trip.scores,
    };
  }

  submitTrip(index: number, queue: tripInfo[]) {
    if (index >= queue.length) {
      this.tripsSubmitted.emit();
      return;
    }

    const tripInfo = queue[index];

    console.log(`Submitting trip ${tripInfo.trip.destination.name}`);
    tripInfo.sent = true;
    tripInfo.previouslySent = true;
    fetch("/api/v1/trips/insert", {
      method: "POST",
      body: JSON.stringify(this.createPostRequest(tripInfo.trip)),
      headers: new Headers({
        "content-type": "application/json",
      }),
    }).then((response) => {
      console.log(
        `Response from back end for ${tripInfo.trip.destination.name}, status: ${response.status}`
      );
      tripInfo.response = response;
      this.submitTrip(index + 1, queue);
      this.tripInfoCache = [...this.tripInfoCache]; // Using this to trigger a re-render
      response.json().then((json) => {
        tripInfo.responseJSON = json;
        console.log(`JSON for ${tripInfo.trip.destination.name}`);
        console.log(json);
        this.tripInfoCache = [...this.tripInfoCache]; // Using this to trigger a re-render
      });
    });
  }

  submit() {
    console.log("Submitting trips to back end");
    if (!this.username || this.username === "") {
      alert("Please enter a name to submit trips");
      return;
    }
    let submitQueue: tripInfo[] = this.tripInfoCache.filter(
      (tripInfo) => !tripInfo.sent
    );

    if (submitQueue.length > 0) {
      this.submitTrip(0, submitQueue);
    }
  }

  render() {
    console.log("Rendering submission page");
    const trips = this.tripInfoCache.map((tripInfo, index) => {
      const modes = tripInfo.trip.scores.map((score) => {
        return (
          <ion-col>
            <ion-label>
              <ion-icon
                size="small"
                slot="start"
                name={score.modeTransport}
              ></ion-icon>
            </ion-label>
            <ion-label>{score.score}</ion-label>
          </ion-col>
        );
      });
      return (
        <ion-item
          class={
            "trip-row" + (this.selectedTripIndex === index ? " selected" : "")
          }
          onClick={() => (this.selectedTripIndex = index)}
        >
          <ion-grid>
            <ion-row>
              <ion-col>
                <ion-label>{tripInfo.trip.destination.name}</ion-label>
              </ion-col>
              {modes}
            </ion-row>
            <ion-row>
              <ion-col>
                <ion-note
                  class={
                    !tripInfo.sent
                      ? "primary"
                      : tripInfo.response?.ok
                      ? "success"
                      : "danger"
                  }
                >
                  {!tripInfo.sent
                    ? "Unsent"
                    : tripInfo.response?.ok
                    ? "Submitted"
                    : "Error"}
                </ion-note>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-item>
      );
    });
    let modesDetail: any[] = undefined;
    let selectedTrip: any[] = undefined;
    if (
      this.tripInfoCache.length > 0 &&
      this.tripInfoCache[this.selectedTripIndex]
    ) {
      modesDetail = this.tripInfoCache[this.selectedTripIndex].trip.scores.map(
        (score) => {
          return (
            <ion-row>
              <ion-col
                sizeXs="8"
                sizeSm="6"
                sizeMd="5"
                sizeLg="5"
                sizeXl="4"
                offset="1"
              >
                <ion-label>
                  {verbs[score.modeTransport]} (
                  <ion-icon size="small" name={score.modeTransport}></ion-icon>
                  ):
                </ion-label>
              </ion-col>
              <ion-col>
                <ion-label>{score.score}</ion-label>
              </ion-col>
            </ion-row>
          );
        }
      );
      selectedTrip = (
        <ion-grid>
          <ion-row>
            <ion-col>
              <ion-label>
                Name:{" "}
                {
                  this.tripInfoCache[this.selectedTripIndex].trip.destination
                    .name
                }
              </ion-label>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col>Transportation Mode Scores:</ion-col>
          </ion-row>
          {modesDetail}
          <ion-row>
            <ion-col>
              Trip Comment:{" "}
              {this.tripInfoCache[this.selectedTripIndex].trip.comment.text}
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col>
              Has this trip been submitted:{" "}
              {this.tripInfoCache[this.selectedTripIndex].sent ? "Yes" : "No"}
            </ion-col>
          </ion-row>
          {this.tripInfoCache[this.selectedTripIndex].response !== undefined
            ? [
                <ion-row>
                  <ion-col>
                    Was there an error:{" "}
                    {!this.tripInfoCache[this.selectedTripIndex].response.ok
                      ? "Yes"
                      : "No"}
                  </ion-col>
                </ion-row>,
                <ion-row>
                  <ion-col>
                    Status:{" "}
                    {this.tripInfoCache[this.selectedTripIndex].response.status}
                  </ion-col>
                </ion-row>,
                this.tripInfoCache[this.selectedTripIndex].responseJSON !==
                undefined ? (
                  <ion-row>
                    <ion-col>
                      Result:{" "}
                      {
                        this.tripInfoCache[this.selectedTripIndex].responseJSON[
                          "result"
                        ]
                      }
                    </ion-col>
                  </ion-row>
                ) : (
                  ""
                ),
              ]
            : ""}
        </ion-grid>
      );
    }
    return (
      <Host>
        <ion-grid>
          <ion-row>
            <ion-col>
              <h3>
                Please enter a name, review your trips, and then hit submit
              </h3>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col sizeXs="2" size="1">
              <ion-label>Name: </ion-label>
            </ion-col>
            <ion-col>
              <ion-textarea
                name="username"
                placeholder="Enter a name"
                value={this.username}
                onIonChange={(event) => this.updateText(event)}
                debounce={300}
              ></ion-textarea>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col>
              <ion-label>Saved Trips</ion-label>
            </ion-col>
          </ion-row>
          <ion-row class="trips">
            <ion-col size="5">
              <div class="scrolly trips">
                <ion-list>{trips}</ion-list>
              </div>
            </ion-col>
            <ion-col size="7" class="trip-info">
              <ion-row>
                <ion-col>Currently Selected Trip</ion-col>
              </ion-row>
              <ion-row>
                <ion-col>{selectedTrip ? selectedTrip : ""}</ion-col>
              </ion-row>
            </ion-col>
          </ion-row>
          <ion-row class="center">
            <ion-col size="4">
              <ion-button expand="block" onClick={() => this.submit()}>
                <ion-label position="fixed">Submit</ion-label>
              </ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>
      </Host>
    );
  }

  componentDidRender() {
    console.log("Submission page has rendered");
  }
}
