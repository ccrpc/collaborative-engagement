import {
  Component,
  Prop,
  Element,
  h,
  Listen,
  Event,
  EventEmitter,
} from "@stencil/core";
import { _t } from "../../i18n/i18n";
import {
  IMapActivity,
  IMapApplication,
  IIntersectionFeature,
} from "../../interfaces";
import { getMap } from "../../utils";
declare const mapboxgl;

/** When the user has selected a home intersection, this activity replaces ce-pretest.
 *
 * In this activity users can view the intersection they have selected as their home and invalidate it, so they must choose a new one, if desired.
 */
@Component({
  tag: "ce-pretest-view",
  styleUrl: "ce-pretest-view.css",
  // shadow: true
})
export class PretestView implements IMapActivity {
  readonly activity_title = _t("this-activity");
  @Element() el: IMapApplication & HTMLGlPopupElement;

  /** The latitude of the center of the map */
  @Prop() latitude: number;

  /** The longitude of the center of the map */
  @Prop() longitude: number;

  /** The zoom level of the center of the map */
  @Prop() zoom: number;

  /** The minimum zoom of the map, user will not be able to zoom out more than this
   * If a tile is needed at a zoom below this point it will neither be requested nor displayed
   */
  @Prop() minzoom: number;

  /** The maximum zoom of the map
   * If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
   */
  @Prop() maxzoom: number;

  /** The index of the current activity, used to refresh the page should the user invalidate their home
   */
  @Prop() current_activity_index: number;

  /** The intersection closes to the user's home */
  @Prop() home_intersection: IIntersectionFeature = undefined;

  /** Used to change / refresh the page should the user invalidate their home */
  @Event() pageChanged: EventEmitter<number>;

  /** Communicates to the root that the user has decided to invalidate their home */
  @Event() pretestSubmitted: EventEmitter<IIntersectionFeature>;

  /** A popup that is attached to the users home intersection and displays information about it */
  private popup = undefined;
  /** Whether or not the popup has been added to the map */
  private added: boolean = false;

  /** This creates the popup, sets its location, and contents
   * The contents is an instance of the ce-pretest-form component
   */
  private createPopup(feature: IIntersectionFeature) {
    const coordinates = feature.geometry.coordinates;
    let component = document.createElement("ce-pretest-form");
    component.features = [feature];
    component.haveSelected = true;

    this.popup = new mapboxgl.Popup({
      maxWidth: "none",
      closeButton: false,
      closeOnClick: false,
    })
      .setLngLat(coordinates)
      .setDOMContent(component);
  }

  /** Update the contents of the popup without creating a new one */
  private updatePopup(feature: IIntersectionFeature) {
    if (!this.popup) {
      return;
    }
    let pretestForm = this.popup.getElement() as HTMLCePretestFormElement;
    pretestForm.features = [feature];
    pretestForm.haveSelected = true;

    const coordinates = feature.geometry.coordinates;
    this.popup.setLngLat(coordinates);
  }

  /** If the component is to be rendered, and the user has a home intersection selected,
   * create the popup if it doesn't exist and update if it does exist.
   */
  componentWillRender() {
    if (this.home_intersection) {
      if (this.popup) {
        this.updatePopup(this.home_intersection);
      } else {
        this.createPopup(this.home_intersection);
      }
    }
  }

  /** After the component, and by extension the map, has been rendered to the screen add the popup to the map
   * if it has not already been added.
   */
  componentDidRender() {
    if (this.popup && !this.added) {
      this.popup.addTo(getMap().map);
      this.added = true;
    }
  }

  componentDidLoad() {
    console.log("pretest view page finished rendering");
  }

  render() {
    return (
      <ce-map
        mapType="pretest-view"
        longitude={this.longitude}
        latitude={this.latitude}
        zoom={this.zoom}
        minzoom={this.minzoom}
        maxzoom={this.maxzoom}
        selected_intersection={this.home_intersection}
      ></ce-map>
    );
  }

  /**
   * Emitted when the user has decided to choose a different intersection for their home location
   * @param e
   */
  @Listen("intersectionSelected", { target: "body" })
  unselectHome(e: CustomEvent<boolean>) {
    this.pretestSubmitted.emit(e.detail ? this.home_intersection : undefined);
    this.pageChanged.emit(this.current_activity_index + (e.detail ? 1 : 0));
  }
}
