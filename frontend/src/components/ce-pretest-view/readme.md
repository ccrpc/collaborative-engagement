# ce-pretest-view



<!-- Auto Generated Below -->


## Properties

| Property                 | Attribute                | Description                                                                                                                                                                        | Type                   | Default     |
| ------------------------ | ------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- | ----------- |
| `current_activity_index` | `current_activity_index` | The index of the current activity, used to refresh the page should the user invalidate their home                                                                                  | `number`               | `undefined` |
| `home_intersection`      | --                       | The intersection closes to the user's home                                                                                                                                         | `IIntersectionFeature` | `undefined` |
| `latitude`               | `latitude`               | The latitude of the center of the map                                                                                                                                              | `number`               | `undefined` |
| `longitude`              | `longitude`              | The longitude of the center of the map                                                                                                                                             | `number`               | `undefined` |
| `maxzoom`                | `maxzoom`                | The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it | `number`               | `undefined` |
| `minzoom`                | `minzoom`                | The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed            | `number`               | `undefined` |
| `zoom`                   | `zoom`                   | The zoom level of the center of the map                                                                                                                                            | `number`               | `undefined` |


## Events

| Event              | Description                                                                 | Type                                |
| ------------------ | --------------------------------------------------------------------------- | ----------------------------------- |
| `pageChanged`      | Used to change / refresh the page should the user invalidate their home     | `CustomEvent<number>`               |
| `pretestSubmitted` | Communicates to the root that the user has decided to invalidate their home | `CustomEvent<IIntersectionFeature>` |


## Dependencies

### Depends on

- [ce-pretest-form](../ce-pretest-form)
- [ce-map](../ce-map)
- ion-thumbnail

### Graph
```mermaid
graph TD;
  ce-pretest-view --> ce-pretest-form
  ce-pretest-view --> ce-map
  ce-pretest-view --> ion-thumbnail
  ce-pretest-form --> ion-item-group
  ce-pretest-form --> ion-item
  ce-pretest-form --> ion-label
  ce-pretest-form --> ion-button
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-button --> ion-ripple-effect
  ce-map --> gl-drawer-toggle
  ce-map --> gl-drawer
  ce-map --> ce-access-score-table
  ce-map --> gl-map
  ce-map --> gl-address-search
  ce-map --> gl-style
  ce-map --> gl-popup
  gl-drawer-toggle --> ion-button
  gl-drawer-toggle --> ion-icon
  gl-drawer-toggle --> gl-drawer
  gl-drawer --> ion-header
  gl-drawer --> ion-toolbar
  gl-drawer --> ion-buttons
  gl-drawer --> ion-title
  gl-drawer --> ion-button
  gl-drawer --> ion-icon
  gl-drawer --> ion-content
  ce-access-score-table --> ion-icon
  gl-address-search --> ion-item
  gl-address-search --> ion-icon
  gl-address-search --> ion-label
  gl-address-search --> ion-list
  gl-address-search --> ion-searchbar
  gl-address-search --> gl-geocode-controller
  ion-searchbar --> ion-icon
  style ce-pretest-view fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
