import { Component, h, Host, Prop } from "@stencil/core";

@Component({
  tag: "ce-welcome",
  styleUrl: "ce-welcome.css",
  shadow: true,
})
export class Welcome {
  @Prop() current_activity_index: number;

  render() {
    return (
      <Host>
        <ion-title>
          Check out the introduction video below on the purpose and function of
          this application.
        </ion-title>
        <br></br>
        <div class="video">
          <iframe
            src="https://www.youtube-nocookie.com/embed/n4a4lGkGt9o?modestbranding=1&origin=cet.ccrpc.org&cc_load_policy=1"
            title="CET Intro"
            frameborder="0"
            allowFullScreen={true}
            loading="lazy"
            seamless={true}
            class="iframe"
          ></iframe>
        </div>
        <br></br>
      </Host>
    );
  }
}
