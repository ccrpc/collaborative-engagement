# ce-destination-intro



<!-- Auto Generated Below -->


## Properties

| Property                 | Attribute                | Description | Type     | Default     |
| ------------------------ | ------------------------ | ----------- | -------- | ----------- |
| `current_activity_index` | `current_activity_index` |             | `number` | `undefined` |


## Dependencies

### Depends on

- ion-title

### Graph
```mermaid
graph TD;
  ce-destination-intro --> ion-title
  style ce-destination-intro fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
