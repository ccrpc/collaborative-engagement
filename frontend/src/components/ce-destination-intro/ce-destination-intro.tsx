import { Component, h, Prop } from "@stencil/core";

@Component({
  tag: "ce-destination-intro",
  styleUrl: "ce-destination-intro.css",
  shadow: true,
})
export class DestinationIntro {
  @Prop() current_activity_index: number;

  // TODO: replace by actual destination map introduction video.
  render() {
    return (
      <div>
        <br></br>
        <ion-title>
          Checkout this introduction video below about how to view the
          destination map in next page.
        </ion-title>
        <br></br>
        {/* <iframe width="1000" height="690" src="https://www.youtube.com/embed/YKh-HqXgZxc?autoplay=0&mute=1"></iframe> */}
        <br></br>
      </div>
    );
  }
}
