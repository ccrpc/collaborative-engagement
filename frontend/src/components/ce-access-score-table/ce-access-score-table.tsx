import { Component, Prop, h } from "@stencil/core";

/** Currently unused */
@Component({
  tag: "ce-access-score-table",
  styleUrl: "ce-access-score-table.css",
  shadow: true,
})
export class AccessScoreTable {
  //this component use fake data to just show idea format of the final result. The table's data should be replaced eventually.

  @Prop() url: string;
  /**
   * JSON returned by the API that contains access scores for 4 transportation modes from your home, your neighborhood and your city.
   *  format: {
   *              "id":...,
   *              "address":...,
   *              "city":...,
   *              "postcode":...,
   *               "destination types": [
   *                 {"name":home
   *                 "score":{"walk":99,...,...,...}},
   *                 {"name":nei..}
   *              ]
   *            }
   */

  // @State() ScoreData: any;

  private ScoreData = [
    { name: "Home", score: { walk: 97, bike: 88, bus: 69, vehicle: 42 } },
    {
      name: "Neighborhood",
      score: { walk: 83, bike: 66, bus: 74, vehicle: 91 },
    },
    {
      name: "Municipality",
      score: { walk: 72, bike: 73, bus: 86, vehicle: 92 },
    },
  ];
  async componentWillLoad() {
    //  let response = await fetch(this.url);
    //  this.ScoreData = await response.json();
  }

  render() {
    // let {Home, Neighborhood, Municipality} = this.ScoreData;
    let transportation_modes = ["walk", "bicycle", "bus", "car"];
    let modes = transportation_modes.map((mode) => {
      return (
        <th>
          <ion-icon size="small" slot="start" name={mode}></ion-icon>
        </th>
      );
    });

    let rows = this.ScoreData.map((row) => {
      return (
        <tr>
          <td class="region">{row.name}</td>
          <td class="score">{row.score.walk}</td>
          <td class="score">{row.score.bike}</td>
          <td class="score">{row.score.bus}</td>
          <td class="score">{row.score.vehicle}</td>
        </tr>
      );
    });

    return (
      <div>
        <h2>Access scores in your neighbors</h2>
        <table>
          <thead>
            <tr>
              <th></th>
              {modes}
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
      </div>
    );
  }
}
