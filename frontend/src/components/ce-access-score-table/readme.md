# ce-access-score-table



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type     | Default     |
| -------- | --------- | ----------- | -------- | ----------- |
| `url`    | `url`     |             | `string` | `undefined` |


## Dependencies

### Used by

 - [ce-map](../ce-map)

### Depends on

- ion-icon

### Graph
```mermaid
graph TD;
  ce-access-score-table --> ion-icon
  ce-map --> ce-access-score-table
  style ce-access-score-table fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
