import {
  Component,
  h,
  Listen,
  Event,
  EventEmitter,
  Prop,
  State,
} from "@stencil/core";
import { _t } from "../../i18n/i18n";
import { IMapApplication, IIntersectionFeature } from "../../interfaces";
import { v4 as uuidv4 } from "uuid";
import Cookies from "js-cookie";
import { ITrip } from "@ccrpc/cet-shared-lib";

export interface tripInfo {
  trip: ITrip;
  sent: boolean;
  previouslySent: boolean;
  response: Response;
  responseJSON: any;
}

/** This is the root of the cet application and will be active at all times. Everything else is seen as a child component of this. */
@Component({
  tag: "ce-root",
  styleUrl: "ce-root.css",
})
export class AppRoot implements IMapApplication {
  /** These are the activities the user will progress through while using this application
   *
   * ce-welcome is both a welcome page as well as having instructions on how to perform the ce-pretest step
   *
   * In ce-pretest the user selects the intersection closest to their home
   *
   * ce-destination-intro is a video with instructions on how to perform the ce-destination step
   *
   * In ce-destination the user will select a destination and rate how easy it is to get to that destination
   * via any of the four transportation modes.
   * The user can select and report on more than one destination during this step, they just need to click on more to get the pop-up
   */
  activities = [
    "ce-welcome",
    "ce-access-score-intro",
    "ce-pretest",
    // "ce-access-score",
    // "ce-destination-intro",
    "ce-destination",
    "ce-submission",
    "ce-wrap-up",
  ];

  /** These are the titles that are displayed at the top of each page
   */

  titles = [
    "Video: Welcome and video",
    "Video: CET Map Walkthrough",
    "Select the intersection closest to your home",
    // "Activity: Access Scores Evaluation",
    // "Video: Demo step 6",
    "Select any number of destinations and the ease of traveling there",
    "Submit Trips",
    "Thank you and contact info",
  ];

  /** Emitted when we want to add to a source in the current map */
  @Event() mapSourceAdd: EventEmitter<ITrip>;

  /** The latitude of the center of the map */
  @Prop() latitude: number;

  /** The longitude of the center of the map */
  @Prop() longitude: number;

  /** The zoom level of the center of the map */
  @Prop() zoom: number;

  /** The minimum zoom of the map, user will not be able to zoom out more than this
   * If a tile is needed at a zoom below this point it will neither be requested nor displayed
   */
  @Prop() minzoom: number;

  /** The maximum zoom of the map
   * If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
   */
  @Prop() maxzoom: number;

  /** Which activity, defined in activities above, the user is currently engaged in */
  @State() current_activity_index: number = 0;

  /** The intersection closest to the user's home, as selected in the ce-pretest activity */
  @State() home_intersection: IIntersectionFeature = undefined;

  /** An event that is triggered when the page is changed */
  @Event() pageChanged: EventEmitter<number>;

  /** An event that signifies the map should toggle the visibility of the glAddressSearch bar
   * Note that having a parent component emit an event that is listened to by a child component
   * goes against the stencil idiom which would have such data passed as a property.
   * That said using a property causes the map to flash which is not desired.
   *
   * The current theory as to why the map flashes is that it is created in a child component and therefore
   * redrawn when any of said child component's Props changes. Other applications that have the map
   * in the root component, such as SSET (gitlab.com/ccrpc/sset/) or LUI (https://gitlab.com/ccrpc/land-use-inventory/), do not have this problem.
   */
  @Event() addressSearchToggle: EventEmitter<void>;

  /** An identifier for this browser, used by the database to identify Users.
   * Stored as a uuid version 4 entity.
   */
  private device_id: string = undefined;

  /** A cache of saved trips to be sent to the database on submit */
  @State() tripInfoCache: tripInfo[] = [];

  /** The user name of the current user */
  @State() username;

  /** The username used for previous submission */
  previousName: string;

  /** Before the root loads get the device_id from a cookie, and if it does not exist then create it and store it in a cookie */
  componentWillLoad() {
    const storedID = Cookies.get("device_id");
    if (storedID) {
      console.log("Device id obtained from cookie");
      this.device_id = storedID;
    } else {
      console.log("Device id cookie not found, generating");
      this.device_id = uuidv4();
      Cookies.set("device_id", this.device_id, { sameSite: "Lax" });
    }
  }

  render() {
    let CurrentActivity = this.activities[this.current_activity_index];
    let title = this.titles[this.current_activity_index];

    const hasMap: boolean =
      this.activities[this.current_activity_index] === "ce-pretest" ||
      this.activities[this.current_activity_index] === "ce-pretest-view" ||
      this.activities[this.current_activity_index] === "ce-destination" ||
      this.activities[this.current_activity_index] === "ce-access-score";

    return (
      <gl-app label={_t(title)} splitPane={true} menu="custom">
        <ion-button
          slot="end-buttons"
          id="searchToggle"
          onClick={() => {
            console.log("Search bar toggle event sent");
            this.addressSearchToggle.emit();
          }}
          style={{ display: hasMap ? "block" : "none" }}
        >
          <ion-icon
            slot="icon-only"
            name="search"
            aria-label="search"
          ></ion-icon>
        </ion-button>
        <CurrentActivity
          longitude={this.longitude}
          latitude={this.latitude}
          zoom={this.zoom}
          minzoom={this.minzoom}
          maxzoom={this.maxzoom}
          current_activity_index={this.current_activity_index}
          device_id={this.device_id}
          home_intersection={this.home_intersection}
          tripInfoCache={this.tripInfoCache}
          username={this.username}
        ></CurrentActivity>
        <ce-comment-board slot="menu" />
        <ion-footer slot="footer">
          <ion-toolbar>
            <ion-buttons slot="primary">
              <ion-button
                onClick={() => this.pageChanged.emit(this.previousPage())}
                color="primary"
              >
                <ion-icon name="arrow-back-outline"></ion-icon>
              </ion-button>
              <ion-button
                onClick={() => this.pageChanged.emit(this.nextPage())}
                color="primary"
              >
                <ion-icon name="arrow-forward-outline"></ion-icon>
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-footer>
      </gl-app>
    );
  }

  /** Change to the previous page, doing nothing if the user is already at the first activity */
  previousPage(): number {
    return Math.max(this.current_activity_index - 1, 0);
  }

  /** Change to the next page, doing nothing if the user is already at the last activity
   * If a user attempts to leave the ce-pretest activity without first selecting a home intersection, alert them and do not change the page
   */
  nextPage(): number {
    if (
      this.activities[this.current_activity_index] === "ce-pretest" &&
      this.home_intersection === undefined
    ) {
      alert("Please select a home intersection before continuing");
      return this.current_activity_index;
    }
    return Math.min(
      this.current_activity_index + 1,
      this.activities.length - 1
    );
  }

  /** Once a user has selected a home intersection ce-pretest becomes ce-pretest-view
   * This activity lets one revisit and change their home intersection (at which point ce-pretest-view is replaced with ce-pretest again)
   */
  @Listen("pretestSubmitted")
  async pretestSubmitted(event: CustomEvent<IIntersectionFeature>) {
    this.home_intersection = event.detail ? event.detail : undefined;

    let pretest_index: number = this.activities.indexOf("ce-pretest");
    if (pretest_index === -1) {
      pretest_index = this.activities.indexOf("ce-pretest-view");
    }
    if (pretest_index !== -1) {
      this.activities[pretest_index] = event.detail
        ? "ce-pretest-view"
        : "ce-pretest";
    }
  }

  /** When  the page changes update the current activity */
  @Listen("pageChanged")
  pageChangedHandler(event: CustomEvent<number>) {
    this.current_activity_index = event.detail;
  }

  @Listen("tripSubmit")
  saveTrip(event: CustomEvent<ITrip>) {
    const trip = event.detail ? event.detail : undefined;
    if (!trip) {
      console.log("Root received empty trip");
    } else {
      console.log(`Root received trip for [${trip.destination.name}]`);
    }
    let push: boolean = true;
    for (const cacheInfo of this.tripInfoCache) {
      if (
        cacheInfo.trip.destination.name === trip.destination.name &&
        cacheInfo.trip.destination.geom.coordinates[0] ===
          trip.destination.geom.coordinates[0] &&
        cacheInfo.trip.destination.geom.coordinates[1] ===
          trip.destination.geom.coordinates[1]
      ) {
        console.log(`Found trip ${trip.destination.name} in cache`);
        cacheInfo.trip = trip;
        cacheInfo.sent = false;
        cacheInfo.previouslySent = false;
        cacheInfo.response = undefined;
        cacheInfo.responseJSON = undefined;
        push = false;
        break;
      }
    }
    if (push) {
      console.log("Trip not found in cache, adding to cache");
      this.tripInfoCache.push({
        trip: trip,
        sent: false,
        previouslySent: false,
        response: undefined,
        responseJSON: undefined,
      });
      this.mapSourceAdd.emit(trip);
    }
  }

  @Listen("usernameChange")
  usernameChange(event: CustomEvent<string>) {
    this.username = event.detail;
    if (
      this.previousName !== undefined &&
      this.username === this.previousName
    ) {
      this.tripInfoCache.map(
        (tripInfo) => (tripInfo.sent = tripInfo.previouslySent)
      );
    } else {
      this.tripInfoCache.map((tripInfo) => {
        tripInfo.sent = false;
      });
    }
  }

  @Listen("tripsSubmitted")
  tripsSubmitted() {
    this.previousName = this.username;
  }
}
