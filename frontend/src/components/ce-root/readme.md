# ce-root



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute   | Description                                                                                                                                                                        | Type     | Default     |
| ----------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | ----------- |
| `latitude`  | `latitude`  | The latitude of the center of the map                                                                                                                                              | `number` | `undefined` |
| `longitude` | `longitude` | The longitude of the center of the map                                                                                                                                             | `number` | `undefined` |
| `maxzoom`   | `maxzoom`   | The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it | `number` | `undefined` |
| `minzoom`   | `minzoom`   | The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed            | `number` | `undefined` |
| `zoom`      | `zoom`      | The zoom level of the center of the map                                                                                                                                            | `number` | `undefined` |


## Events

| Event                 | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | Type                  |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- |
| `addressSearchToggle` | An event that signifies the map should toggle the visibility of the glAddressSearch bar Note that having a parent component emit an event that is listened to by a child component goes against the stencil idiom which would have such data passed as a property. That said using a property causes the map to flash which is not desired.  The current theory as to why the map flashes is that it is created in a child component and therefore redrawn when any of said child component's Props changes. Other applications that have the map in the root component, such as SSET (gitlab.com/ccrpc/sset/) or LUI (https://gitlab.com/ccrpc/land-use-inventory/), do not have this problem. | `CustomEvent<void>`   |
| `mapSourceAdd`        | Emitted when we want to add to a source in the current map                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | `CustomEvent<ITrip>`  |
| `pageChanged`         | An event that is triggered when the page is changed                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | `CustomEvent<number>` |


## Dependencies

### Depends on

- gl-app
- ion-button
- ion-icon
- [ce-comment-board](../ce-comment-board)
- ion-footer
- ion-toolbar
- ion-buttons

### Graph
```mermaid
graph TD;
  ce-root --> gl-app
  ce-root --> ion-button
  ce-root --> ion-icon
  ce-root --> ce-comment-board
  ce-root --> ion-footer
  ce-root --> ion-toolbar
  ce-root --> ion-buttons
  gl-app --> ion-menu
  gl-app --> ion-header
  gl-app --> ion-toolbar
  gl-app --> ion-title
  gl-app --> ion-content
  gl-app --> ion-menu-toggle
  gl-app --> ion-button
  gl-app --> ion-icon
  gl-app --> ion-buttons
  gl-app --> ion-footer
  gl-app --> ion-split-pane
  gl-app --> ion-app
  ion-menu --> ion-backdrop
  ion-button --> ion-ripple-effect
  ce-comment-board --> ion-list
  ce-comment-board --> ion-list-header
  ce-comment-board --> ion-label
  ce-comment-board --> ce-comment
  ce-comment --> ion-col
  ce-comment --> ion-label
  ce-comment --> ion-icon
  ce-comment --> ion-card
  ce-comment --> ion-card-header
  ce-comment --> ion-card-subtitle
  ce-comment --> ion-card-content
  ce-comment --> ion-grid
  ce-comment --> ion-row
  ion-card --> ion-ripple-effect
  style ce-root fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
