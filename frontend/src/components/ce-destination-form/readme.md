# ce-destination-form



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute | Description                                                                          | Type                     | Default     |
| --------------- | --------- | ------------------------------------------------------------------------------------ | ------------------------ | ----------- |
| `features`      | --        | The features the user clicked on to generate this popup.  Passed along via webmapgl. | `MapBoxGLFeature<any>[]` | `undefined` |
| `tripInfoCache` | --        | A cache of already saved trips                                                       | `tripInfo[]`             | `undefined` |


## Events

| Event                      | Description                                                                                                           | Type                 |
| -------------------------- | --------------------------------------------------------------------------------------------------------------------- | -------------------- |
| `destinationFormSubmitted` | An event emitted when the user has entered and submitted information about the destination this popup was created for | `CustomEvent<ITrip>` |


## Dependencies

### Depends on

- ion-item
- ion-label
- ion-item-group
- ion-list
- ion-checkbox
- ion-icon
- ion-range
- ion-textarea
- ion-button

### Graph
```mermaid
graph TD;
  ce-destination-form --> ion-item
  ce-destination-form --> ion-label
  ce-destination-form --> ion-item-group
  ce-destination-form --> ion-list
  ce-destination-form --> ion-checkbox
  ce-destination-form --> ion-icon
  ce-destination-form --> ion-range
  ce-destination-form --> ion-textarea
  ce-destination-form --> ion-button
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-button --> ion-ripple-effect
  style ce-destination-form fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
