import { Component, h, Prop, Event, EventEmitter, State } from "@stencil/core";
import {
  CheckboxChangeEventDetail,
  RangeChangeEventDetail,
  TextareaChangeEventDetail,
} from "@ionic/core";
import { Point } from "geojson";
import { MapBoxGLFeature, IDestinationFeature } from "../../interfaces";
import { ModeTransportation, IScore, ITrip } from "@ccrpc/cet-shared-lib";
import { tripInfo } from "../ce-root/ce-root";

const verbs: { [key in ModeTransportation]: string } = {
  [ModeTransportation.WALK]: "walking",
  [ModeTransportation.BIKE]: "biking",
  [ModeTransportation.BUS]: "taking a bus",
  [ModeTransportation.DRIVE]: "driving",
};

/** When the user is in the ce-destination activity and selects a destination, this component is generated in a popup to allow them to score
 * that destination.
 */
@Component({
  tag: "ce-destination-form",
  styleUrl: "ce-destination-form.css",
  // shadow: true
})
export class DestinationForm {
  /** An event emitted when the user has entered and submitted information about the destination this popup was created for */
  @Event() destinationFormSubmitted: EventEmitter<ITrip>;

  /** The features the user clicked on to generate this popup.
   *
   * Passed along via webmapgl.
   */
  @Prop() features: MapBoxGLFeature<any>[];

  /** A cache of already saved trips */
  @Prop() tripInfoCache: tripInfo[];

  /** The scores the user has asigned to each of the different movement modes.
   *
   * Does only contain information about the movement modes actually reported on.
   */
  @State() scores: { [key in ModeTransportation]?: number } = {};

  /** Which of the four modes the user is reporting on */
  @State() reportingModes: { [key in ModeTransportation]: boolean };

  /** If there are multiple destinations at this location, display information for this one */
  @State() destinationChoice: number = 0;

  /** The comment the user gives to the trip
   * This is not a @ State to prevent redundant renders.
   */
  userComment: string = "";

  /** Whether there are multiple destinations at this location */
  multipleDestinations: boolean = false;

  /** Whether this component has already checked whether it was in the cache (this way we do not run the for loop multiple times) */
  checkedCache: boolean = false;

  /** The four different types of transportation the application (front end and back end) recognizes */
  static readonly transportation_modes: string[] = [
    "walk",
    "bicycle",
    "bus",
    "car",
  ];

  /** This combines the information the user entered with the destination id of the popup to create the trips to pass along to the database */
  sendData(destinationFeature: IDestinationFeature): ITrip {
    // using "_geometry" as "geometry" seems to not be defined yet.
    const destinationGeom: Point = {
      type: "Point",
      coordinates: [
        destinationFeature._geometry.coordinates[0],
        destinationFeature._geometry.coordinates[1],
      ],
    };

    const scoreArray: Map<string, number> = new Map(
      DestinationForm.transportation_modes
        .filter((mode: ModeTransportation) => this.reportingModes[mode])
        .map((mode: ModeTransportation) => [mode, this.scores[mode]])
    );

    let scores: IScore[] = [];
    scoreArray.forEach((value, key) => {
      let modeTransport: ModeTransportation;
      switch (key) {
        case "walk":
          modeTransport = ModeTransportation.WALK;
          break;
        case "bicycle":
          modeTransport = ModeTransportation.BIKE;
          break;
        case "bus":
          modeTransport = ModeTransportation.BUS;
          break;
        case "car":
          modeTransport = ModeTransportation.DRIVE;
          break;
        default:
          console.log("Unknown mode of transportation: ", key);
          return;
      }
      scores.push({
        modeTransport: modeTransport,
        score: value ? value : 50,
        _id: undefined,
        trip: undefined,
      });
    });

    return {
      _id: undefined,
      dateCreated: undefined,
      dateModified: undefined,
      user: undefined,
      comment: {
        _id: undefined,
        datePublished: undefined,
        dateModified: undefined,
        poster: undefined,
        trip: undefined,
        text: this.userComment,
      },
      origin: undefined,
      destination: {
        _id: undefined,
        type: destinationFeature.properties.type,
        category: destinationFeature.properties.category,
        name: destinationFeature.properties.name,
        geom: destinationGeom,
      },
      scores: scores,
    };
  }

  /** Called when the user checks or un-checks a movement mode */
  modeSelect(mode: string, e: CustomEvent<CheckboxChangeEventDetail>) {
    this.reportingModes[mode] = e.detail.checked;
    this.reportingModes = { ...this.reportingModes }; // Used to trigger a re-render.
  }

  /** Simple event to update the scores when the user moves the slider */
  updateScore(mode: string, event: CustomEvent<RangeChangeEventDetail>) {
    this.scores[mode] = event.detail.value as number;
  }

  /** Update the user comment */
  updateText(event: CustomEvent<TextareaChangeEventDetail>) {
    this.userComment = event.detail.value;
  }

  /** If this point has more than one destination, have the user choose which one to display / report on */
  chooseDestination(): any {
    const destinations = this.features.map((dest, index, destArray) => {
      const destination: IDestinationFeature = dest as IDestinationFeature;
      return (
        <ion-item lines={index === destArray.length - 1 ? "none" : "full"}>
          <ion-label
            style={{ cursor: "pointer" }}
            onClick={() => (this.destinationChoice = index)}
          >
            {destination.properties.name} ({destination.properties.type})
          </ion-label>
        </ion-item>
      );
    });

    return (
      <ion-item-group>
        <ion-item>
          <ion-label>
            There are multiple destinations at this location, please select one.
          </ion-label>
        </ion-item>
        <ion-item>
          <ion-list>{destinations}</ion-list>
        </ion-item>
      </ion-item-group>
    );
  }

  /** Check whether this destination is already in the cache of saved trips and if so load in that data so that we can update it rather than re-writing it */
  checkAndLoad(destination: IDestinationFeature): void {
    if (this.checkedCache) return;
    for (const trip of this.tripInfoCache) {
      if (
        trip.trip.destination.name === destination.properties.name &&
        trip.trip.destination.geom.coordinates[0] ==
          destination._geometry.coordinates[0] &&
        trip.trip.destination.geom.coordinates[1] ===
          destination._geometry.coordinates[1]
      ) {
        for (const score of trip.trip.scores) {
          this.scores[score.modeTransport] = score.score;
          this.reportingModes[score.modeTransport] = true;
        }
        this.userComment = trip.trip.comment.text;
        break;
      }
    }
    this.checkedCache = true;
  }

  /** Create the reporting modes object and set all if its base values to false */
  componentWillLoad() {
    this.reportingModes = {
      walk: false,
      bicycle: false,
      bus: false,
      car: false,
    };

    if (this.features.length > 1) {
      this.multipleDestinations = true;
      this.destinationChoice = -1;
    } else {
      this.checkAndLoad(this.features[0] as IDestinationFeature);
    }
  }

  componentWillRender() {
    console.log("Destination Form Rendering");
  }

  render() {
    if (this.multipleDestinations && this.destinationChoice === -1) {
      return this.chooseDestination();
    }
    const destination: IDestinationFeature = this.features[
      this.destinationChoice
    ] as IDestinationFeature;
    this.checkAndLoad(destination);
    const checkbox = DestinationForm.transportation_modes.map((mode) => {
      return (
        <td class="checkbox">
          <ion-checkbox
            name={`ch_${mode}`}
            onIonChange={(event) => this.modeSelect(mode, event)}
            checked={this.reportingModes[mode]}
          ></ion-checkbox>
        </td>
      );
    });
    const modeIcons = DestinationForm.transportation_modes.map((mode) => {
      return (
        <th class="icon">
          <ion-icon size="small" slot="start" name={mode}></ion-icon>
          <ion-label>{mode === "car" ? "drive" : mode}</ion-label>
        </th>
      );
    });
    const sliders = DestinationForm.transportation_modes
      .filter((mode) => this.reportingModes[mode])
      .map(function (mode) {
        return (
          <ion-item>
            <div style={{ width: "100%" }}>
              <ion-label class="ion-text-wrap" style={{ "max-width": "100%" }}>
                What is the ease of {verbs[mode]} to this location?
              </ion-label>
              <ion-range
                min={0}
                max={100}
                pin={true}
                value={this.scores[mode] !== undefined ? this.scores[mode] : 50}
                name={`sl_${mode}`}
                onIonChange={(event) => this.updateScore(mode, event)}
              >
                <ion-icon slot="start" name={mode}></ion-icon>
                <ion-label slot="start">
                  {mode === "car" ? "drive" : mode}
                </ion-label>
                <ion-label slot="start">0</ion-label>
                <ion-label slot="end">100</ion-label>
              </ion-range>
            </div>
          </ion-item>
        );
      }, this);
    const text_rows = 3;
    return (
      <form id="destinationForm" method="" onSubmit={() => false}>
        <ion-item-group>
          <ion-item>
            <ion-label>
              {destination.properties.name} ({destination.properties.type})
            </ion-label>
          </ion-item>
          <ion-item>
            <ion-label class="ion-text-wrap">
              How easy is it to use each transportation method to get to this
              location from the intersection closest to your home?
              <p>0 = very difficult, 100 = very easy</p>
            </ion-label>
          </ion-item>
          <ion-item>
            <ion-label>Select one or more transportation methods</ion-label>
          </ion-item>

          <table>
            <thead>
              <tr>{modeIcons}</tr>
            </thead>
            <tbody>
              <tr>{checkbox}</tr>
            </tbody>
          </table>

          {sliders}
          <ion-item>
            <ion-label position="stacked">Any additional comments:</ion-label>
            <ion-textarea
              name="textarea"
              rows={text_rows}
              value={this.userComment}
              placeholder="Enter your comments here..."
              onIonChange={(event) => this.updateText(event)}
              debounce={300}
            ></ion-textarea>
          </ion-item>
        </ion-item-group>
        <br></br>
        <ion-button
          expand="block"
          onClick={() =>
            this.destinationFormSubmitted.emit(this.sendData(destination))
          }
        >
          <ion-label position="fixed">Save</ion-label>
        </ion-button>
      </form>
    );
  }

  componentDidRender() {
    console.log("Destination Form has rendered");
  }
}
