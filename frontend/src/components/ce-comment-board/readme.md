# ce-comment-board



<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [ce-root](../ce-root)

### Depends on

- ion-list
- ion-list-header
- ion-label
- [ce-comment](../ce-comment)

### Graph
```mermaid
graph TD;
  ce-comment-board --> ion-list
  ce-comment-board --> ion-list-header
  ce-comment-board --> ion-label
  ce-comment-board --> ce-comment
  ce-comment --> ion-col
  ce-comment --> ion-label
  ce-comment --> ion-icon
  ce-comment --> ion-card
  ce-comment --> ion-card-header
  ce-comment --> ion-card-subtitle
  ce-comment --> ion-card-content
  ce-comment --> ion-grid
  ce-comment --> ion-row
  ion-card --> ion-ripple-effect
  ce-root --> ce-comment-board
  style ce-comment-board fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
