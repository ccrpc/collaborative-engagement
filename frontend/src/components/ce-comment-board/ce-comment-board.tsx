import { Component, Host, h, State, Listen } from "@stencil/core";
import { IComment } from "@ccrpc/cet-shared-lib/models/comment";

@Component({
  tag: "ce-comment-board",
  styleUrl: "ce-comment-board.css",
  shadow: true,
})
export class CommentBoard {
  @State() comments: IComment[] = [];

  async loadComments() {
    const resp = await fetch("api/v1/comments/list/50");
    this.comments = await resp.json();
  }

  async componentWillRender() {
    await this.loadComments();
  }

  render() {
    return (
      <Host style={{ flexDirection: "column" }}>
        <ion-list style={{ overflowY: "scroll", overflowX: "hide" }}>
          <ion-list-header>
            <ion-label>Comment Board</ion-label>
          </ion-list-header>
          {this.comments.map((comment) => {
            return (
              <ce-comment
                _id={comment._id}
                text={comment.text}
                datePublished={comment.datePublished}
                dateModified={comment.dateModified}
                poster={comment.poster}
                trip={comment.trip}
              ></ce-comment>
            );
          })}
        </ion-list>
      </Host>
    );
  }

  @Listen("tripsSubmitted", { target: "body" })
  async refreshComments() {
    this.loadComments();
  }
}
