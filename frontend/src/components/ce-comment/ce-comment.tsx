import { Component, Host, h, Prop } from "@stencil/core";
import { IComment, IUser, ITrip } from "@ccrpc/cet-shared-lib";

@Component({
  tag: "ce-comment",
  styleUrl: "ce-comment.css",
  shadow: true,
})
export class Comment implements IComment {
  @Prop() _id: number;
  @Prop() text: string;
  @Prop() datePublished: Date;
  @Prop() dateModified: Date;
  @Prop() poster: IUser;
  @Prop() trip: ITrip;

  // TODO: Incorperate other fields and style comments
  render() {
    const scoresRow = this.trip.scores.map((score) => {
      return (
        <ion-col>
          <ion-label>
            <ion-icon
              size="small"
              slot="start"
              name={score.modeTransport}
            ></ion-icon>
          </ion-label>
          <ion-label>: {score.score}</ion-label>
        </ion-col>
      );
    });
    return (
      <Host>
        <ion-card>
          <ion-card-header style={{ background: "grey" }}>
            <ion-card-subtitle>
              <ion-label style={{ color: "white" }}>
                {this.poster.username}: {this.trip.destination.name}
              </ion-label>
            </ion-card-subtitle>
          </ion-card-header>
          <ion-card-content>
            <ion-grid>
              <ion-row>
                <ion-col>Scores:</ion-col>
                {scoresRow}
              </ion-row>
              <ion-row>
                <ion-col>{this.text}</ion-col>
              </ion-row>
            </ion-grid>
          </ion-card-content>
        </ion-card>
      </Host>
    );
  }
}
