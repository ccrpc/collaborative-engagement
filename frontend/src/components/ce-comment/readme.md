# ce-comment



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute | Description | Type     | Default     |
| --------------- | --------- | ----------- | -------- | ----------- |
| `_id`           | `_id`     |             | `number` | `undefined` |
| `dateModified`  | --        |             | `Date`   | `undefined` |
| `datePublished` | --        |             | `Date`   | `undefined` |
| `poster`        | --        |             | `IUser`  | `undefined` |
| `text`          | `text`    |             | `string` | `undefined` |
| `trip`          | --        |             | `ITrip`  | `undefined` |


## Dependencies

### Used by

 - [ce-comment-board](../ce-comment-board)

### Depends on

- ion-col
- ion-label
- ion-icon
- ion-card
- ion-card-header
- ion-card-subtitle
- ion-card-content
- ion-grid
- ion-row

### Graph
```mermaid
graph TD;
  ce-comment --> ion-col
  ce-comment --> ion-label
  ce-comment --> ion-icon
  ce-comment --> ion-card
  ce-comment --> ion-card-header
  ce-comment --> ion-card-subtitle
  ce-comment --> ion-card-content
  ce-comment --> ion-grid
  ce-comment --> ion-row
  ion-card --> ion-ripple-effect
  ce-comment-board --> ce-comment
  style ce-comment fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
