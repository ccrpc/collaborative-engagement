import { Component, h, Host, Prop } from "@stencil/core";

/** Currently unused */
@Component({
  tag: "ce-access-score-intro",
  styleUrl: "ce-access-score-intro.css",
  shadow: true,
})
export class AccessScoreIntro {
  @Prop() current_activity_index: number;

  render() {
    return (
      <Host>
        <ion-title>
          Check out this introduction video below about how to report
          destination accessibility.
        </ion-title>
        <br></br>
        <div class="video">
          <iframe
            src="https://www.youtube-nocookie.com/embed/BNW13QEzL3g?modestbranding=1&origin=cet.ccrpc.org&cc_load_policy=1"
            title="CET Map Walkthrough"
            frameborder="0"
            allowFullScreen={true}
            loading="lazy"
            seamless={true}
            class="iframe"
          ></iframe>
        </div>
        <br></br>
      </Host>
    );
  }
}
