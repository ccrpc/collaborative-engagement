# ce-map



<!-- Auto Generated Below -->


## Properties

| Property                | Attribute   | Description                                                                                                                                                                        | Type                                                             | Default     |
| ----------------------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `latitude`              | `latitude`  | The latitude of the center of the map                                                                                                                                              | `number`                                                         | `undefined` |
| `longitude`             | `longitude` | The longitude of the center of the map                                                                                                                                             | `number`                                                         | `undefined` |
| `mapType`               | `map-type`  | What type of map to render                                                                                                                                                         | `"access-score" \| "destination" \| "pretest" \| "pretest-view"` | `undefined` |
| `maxzoom`               | `maxzoom`   | The maximum zoom of the map If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it | `number`                                                         | `undefined` |
| `minzoom`               | `minzoom`   | The minimum zoom of the map, user will not be able to zoom out more than this If a tile is needed at a zoom below this point it will neither be requested nor displayed            | `number`                                                         | `undefined` |
| `selected_intersection` | --          | The intersection to be highlighted                                                                                                                                                 | `IIntersectionFeature`                                           | `undefined` |
| `tripInfoCache`         | --          | A cache of the already saved trips                                                                                                                                                 | `tripInfo[]`                                                     | `undefined` |
| `zoom`                  | `zoom`      | The zoom level of the center of the map                                                                                                                                            | `number`                                                         | `undefined` |


## Dependencies

### Used by

 - [ce-access-score](../ce-access-score)
 - [ce-destination](../ce-destination)
 - [ce-pretest](../ce-pretest)
 - [ce-pretest-view](../ce-pretest-view)

### Depends on

- gl-drawer-toggle
- gl-drawer
- [ce-access-score-table](../ce-access-score-table)
- gl-map
- gl-address-search
- gl-style
- gl-popup

### Graph
```mermaid
graph TD;
  ce-map --> gl-drawer-toggle
  ce-map --> gl-drawer
  ce-map --> ce-access-score-table
  ce-map --> gl-map
  ce-map --> gl-address-search
  ce-map --> gl-style
  ce-map --> gl-popup
  gl-drawer-toggle --> ion-button
  gl-drawer-toggle --> ion-icon
  gl-drawer-toggle --> gl-drawer
  ion-button --> ion-ripple-effect
  gl-drawer --> ion-header
  gl-drawer --> ion-toolbar
  gl-drawer --> ion-buttons
  gl-drawer --> ion-title
  gl-drawer --> ion-button
  gl-drawer --> ion-icon
  gl-drawer --> ion-content
  ce-access-score-table --> ion-icon
  gl-address-search --> ion-item
  gl-address-search --> ion-icon
  gl-address-search --> ion-label
  gl-address-search --> ion-list
  gl-address-search --> ion-searchbar
  gl-address-search --> gl-geocode-controller
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-searchbar --> ion-icon
  ce-access-score --> ce-map
  ce-destination --> ce-map
  ce-pretest --> ce-map
  ce-pretest-view --> ce-map
  style ce-map fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
