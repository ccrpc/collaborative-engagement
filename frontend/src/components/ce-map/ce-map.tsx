import { Component, Prop, Element, h, Listen } from "@stencil/core";
import { webMapLayers } from "../../assets/styles/layers";
import { webMapSources } from "../../assets/styles/sources";
import {
  IIntersectionFeature,
  IMapActivity,
  IMapApplication,
} from "../../interfaces";
import { _t } from "../../i18n/i18n";
import { tripInfo } from "../ce-root/ce-root";

interface styleJSON {
  version: number;
  sprite: string;
  glyphs: string;
  sources: typeof webMapSources["access-score"] | typeof webMapSources.trips;
  layers: any[];
}

/** This component defines the gl-map to be used by each activity.
 *
 * This includes highlighting the intersection the user has set as the closest to their home.
 */
@Component({
  tag: "ce-map",
  styleUrl: "ce-map.css",
  // shadow: true
})
export class Map implements IMapActivity {
  readonly activity_title = _t("this-activity");
  @Element() el: IMapApplication & HTMLElement;

  /** What type of map to render */
  @Prop() mapType: "pretest" | "pretest-view" | "access-score" | "destination";

  /** The latitude of the center of the map */
  @Prop() latitude: number;

  /** The longitude of the center of the map */
  @Prop() longitude: number;

  /** The zoom level of the center of the map */
  @Prop() zoom: number;

  /** The minimum zoom of the map, user will not be able to zoom out more than this
   * If a tile is needed at a zoom below this point it will neither be requested nor displayed
   */
  @Prop() minzoom: number;

  /** The maximum zoom of the map
   * If the user requests a tile at a higher zoom, then the tile from the maxzoom level will be requested and the application will attempt to "overzoom" it
   */
  @Prop() maxzoom: number;

  /** The intersection to be highlighted */
  @Prop() selected_intersection: IIntersectionFeature = undefined;

  /** A cache of the already saved trips */
  @Prop() tripInfoCache: tripInfo[];

  /** This highlights an intersection by modifying the intersection layer's "circle-color" property.
   *
   * For more information see frontend/src/assets/styles/layers.tsx
   *
   * @param {typeof webMapLayers.intersections} intersectionLayers - The intersection layers object to edit.
   */
  highlightSelectedIntersection(
    intersectionLayers: typeof webMapLayers.intersections
  ): void {
    let intersectionData = intersectionLayers.find(
      (i) => (i.id = "intersection-data")
    );
    if (intersectionData) {
      let selection: number = this.selected_intersection?.properties
        ?.intersection_id
        ? this.selected_intersection.properties.intersection_id
        : -1;
      intersectionData.paint["circle-color"] = [
        "let",
        "selected",
        selection,
        ...intersectionData.paint["circle-color"].splice(3),
      ];
    }
  }

  /**
   * Removes the non-selected intersections from the eventual map
   *
   * @param {typeof webMapLayers.intersections} intersectionLayers - The intersection layers object to edit.
   * @param {number} id The intersection to display
   * @param {boolean} [apply=true] Whether to apply or remove the filter.
   */
  applyFilter(
    intersectionLayers: typeof webMapLayers.intersections,
    id: number,
    apply = true
  ): void {
    let intersectionData = intersectionLayers.find(
      (i) => (i.id = "intersection-data")
    );
    if (intersectionData) {
      if (apply) {
        intersectionData["filter"] = ["==", ["get", "intersection_id"], id];
      } else {
        delete intersectionData["filter"];
      }
    }
  }

  /**
   * The three pieces of information the map will need.
   * @typedef {Object} mapInfo
   * @property {Object} json The json used by mapbox gl, including both sources and layer display definitions
   * @property {(string|string[])} layer The list of clickable layers, or a string if there is only one clickable layer
   * @property {string} popupForm The name of the component the map will use as popups.
   */

  /** This constructs the data to be used in the gl-style element in the map
   *
   * @param {string} mapType - The type of map to render
   * @return {mapInfo} The information used to initialize the map.
   */
  getData(mapType: string): {
    json: styleJSON;
    layer: string | string[];
    popupForm: string;
  } {
    const baseJSON = {
      version: 8,
      sprite: "https://ccrpc.gitlab.io/access-score/assets/style/sprite",
      glyphs: "https://maps.ccrpc.org/fonts/{fontstack}/{range}.pbf",
    };

    let json: styleJSON = null;
    let layer: string | string[] = null;
    let popupForm: string = null;

    let intersections = [...webMapLayers.intersections];
    this.highlightSelectedIntersection(intersections);
    if (mapType === "destination" || mapType === "pretest-view") {
      let selection: number = this.selected_intersection?.properties
        ?.intersection_id
        ? this.selected_intersection.properties.intersection_id
        : -1;
      this.applyFilter(intersections, selection, selection !== -1);
    } else {
      this.applyFilter(intersections, -1, false);
    }

    if (mapType == "pretest-view") {
      popupForm = "ce-pretest-form";
      json = {
        ...baseJSON,
        sources: webMapSources["access-score"],
        layers: intersections,
      };
    } else if (mapType == "pretest") {
      popupForm = "ce-pretest-form";
      layer = "intersection-data";
      json = {
        ...baseJSON,
        sources: webMapSources["access-score"],
        layers: intersections,
      };
    } else if (mapType == "access-score") {
      json = {
        ...baseJSON,
        sources: webMapSources["access-score"],
        layers: webMapLayers.segments,
      };
    } else {
      /* mapType === destination */
      popupForm = "ce-destination-form";
      layer = "destination-click";
      json = {
        ...baseJSON,
        sources: { ...webMapSources["access-score"], ...webMapSources.trips },
        layers: [
          ...webMapLayers.destinations,
          ...intersections,
          ...webMapLayers.trips,
        ],
      };
    }
    return { json, layer, popupForm };
  }

  splitPanel() {
    if (this.mapType == "access-score") {
      return [
        <gl-drawer-toggle buttonTitle="button"></gl-drawer-toggle>,
        <gl-drawer drawerTitle="access-score-table" mapId={this.el.id}>
          <ce-access-score-table></ce-access-score-table>
        </gl-drawer>,
      ];
    }
  }

  render() {
    console.log("Map Rendering");
    let data = this.getData(this.mapType);

    return (
      <gl-map
        longitude={this.longitude}
        latitude={this.latitude}
        zoom={this.zoom}
        minzoom={this.minzoom}
        maxzoom={this.maxzoom}
        hash={true}
        clickMode="all"
      >
        <gl-address-search
          id="searchBar"
          url="https://geocode.ccrpc.org/search.php"
          bbox={[-88.462126, 39.879098, -87.931727, 40.399428]}
          style={{ display: "block" }}
        ></gl-address-search>
        <gl-style
          id="intersections"
          json={data.json}
          enabled={true}
          clickableLayers={data.layer}
        >
          <gl-popup
            component={data.popupForm}
            layers={data.layer}
            componentOptions={{ tripInfoCache: this.tripInfoCache }}
          ></gl-popup>
        </gl-style>
        <gl-style
          url="https://maps.ccrpc.org/basemaps/basic/style.json"
          id="basemap"
          basemap={true}
          enabled={true}
        ></gl-style>
        {this.splitPanel()}
      </gl-map>
    );
  }

  componentDidRender() {
    console.log("Map has rendered");
  }

  @Listen("addressSearchToggle", { target: "body" })
  toggleSearchBar() {
    let bar = document.getElementById("searchBar");
    if (bar) {
      bar.style.display = bar.style.display === "block" ? "none" : "block";
    }
  }
}
