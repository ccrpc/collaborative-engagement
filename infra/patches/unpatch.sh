#!/bin/bash


microk8s kubectl -n ingress patch configmap/nginx-ingress-tcp-microk8s-conf --patch-file /opt/code/collaborative-engagement/infra/patches/ingress-tcp-teardown.yaml

CONTAINER_INDEX=$(kubectl -n ingress get daemonset/nginx-ingress-microk8s-controller -o json  | jq '.spec.template.spec.containers | map(.name == "nginx-ingress-microk8s") | index(true)')
PORT_INDEX=$(kubectl -n ingress get daemonset/nginx-ingress-microk8s-controller -o json  | jq '.spec.template.spec.containers[] | select(.name == "nginx-ingress-microk8s") | .ports | map(.name == "cet-postgis") | index(true)')
if [ $PORT_INDEX == "null" ] || [ $PORT_INDEX == "null" ];
then echo "Port not found";
else microk8s kubectl -n ingress patch daemonset nginx-ingress-microk8s-controller --type=json -p="[{'op': 'remove', 'path': '/spec/template/spec/containers/$CONTAINER_INDEX/ports/$PORT_INDEX'}]";
fi
