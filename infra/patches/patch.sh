#!/bin/bash

microk8s kubectl -n ingress patch configmap/nginx-ingress-tcp-microk8s-conf --patch-file /opt/code/collaborative-engagement/infra/patches/ingress-tcp-setup.yaml
microk8s kubectl -n ingress patch daemonset nginx-ingress-microk8s-controller --patch-file /opt/code/collaborative-engagement/infra/patches/ingress-nginx-controller-setup.yaml