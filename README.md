Note: Information on how to configure the kubernetes settings for the project are in README files in /infra/config and /infra/config/prod-certs.

One thing to mention in addition to those is that the postgis instance of this project is currently accessible on cloud1 on the port 5436.

This was achieved by opening up the tcp port on the cloud1 ingress. Now ingress objects do not normally allow one to open TCP ports,
the nginx-ingress controller we are using does. This is via editing a configmap and the definition of the daemonset itself.

`configmap/nginx-ingress-tcp-microk8s-conf`

`daemonset/nginx-ingress-microk8s-controller`

Editing the configmap causes the ingress controller to reload automatically. Therefore one does not need to spin the ingress down and then spin it up again.

For instructions on how to edit these things see: https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/

Specifically you will want to add the following to the configmap and daemonset.

`configmap/nginx-ingress-tcp-microk8s-conf`

```yaml
data:
  "5436": collaborative-engagement/cet-postgis-svc:5432
```

`daemonset/nginx-ingress-microk8s-controller`

```yaml
ports:
  - containerPort: 5436
    hostPort: 5436
    name: cet-postgis
    protocol: TCP
```

Methods for doing so can be found in the /infra/patches folder.

To edit the files manually use `KUBE_EDITOR=nano k1 -n ingress edit daemonset/nginx-ingress-microk8s-controller` and `KUBE_EDITOR=nano k1 -n ingress edit configmap/nginx-ingress-tcp-microk8s-conf`.

Note: This is the first time a tcp port is being exposed on cloud1. If we do this more in the future we may find a better way to do it / better way to keep track of what ports are mapped to what.
