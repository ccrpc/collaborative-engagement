# Collaborative Engagement

# Introduction

<!-- TODO Update list of technologies -->

This project is a real-time application in order to engage participants and presenters in a meeting and record some of the meeting contents for CCRPC.
The app is built using Stencil, Node.js, Express, Sequelize, PostgresSQL, PostGIS, and Redis

# Description

Currently the back-end consists of a postGIS database that handles user result and comments. Planned is a Redis instance to cache results.

## Entities and the Accessibility Script

The typeorm database includes entities for the results of the [accessibility script](https://gitlab.com/ccrpc/cuuats.snt.accessibility).

The accessibility script creates a network via the Pandana tool and analyzes how difficult it is to get to different categories of destination (e.g. art, park, health, job) via different transportation modes (e.g. walk, drive).
This is based on the Level of Traffic Stress.

What this tool asks of the user is just to rate the ease of access of just the type of transportation (eg. bike, drive, walk), so for comparison purposes the different categories are averaged to their transportation modes.

This comparison can be seen via the PLACEHOLDER view entities.

### Uploading data

The data from the accessibility script is uploaded to the CET database by running `yarn upload-mbtiles` while in the back-end.

The app has to be running for this, either via skaffold dev or up in productions mode.

`yarn upload-mbtiles` works by creating a Kubernetes Job that uses ogr2ogr to upload the data in the /tiles/pois/pois_access.mbtiles.
This Job can be inspected (and altered) via the /tiles/upload-mbtiles.yaml.
