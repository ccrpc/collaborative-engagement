import express, { Request, response, Response } from "express";
import bodyParser from "body-parser";
import { IComment, IPostTripsRequest, IUser } from "@ccrpc/cet-shared-lib";
import { dataSource } from "./data-source";
import { Comment } from "./database/entity/Comment";
import { Trip } from "./database/entity/Trip";
import { User } from "./database/entity/User";
import { EntityNotFoundError } from "typeorm";
import { Score } from "./database/entity/Score";
import { Intersection } from "./database/entity/Intersection";
import { Destination } from "./database/entity/Destination";
// TODO: import logging client (Fluentd, maybe...)

dataSource
  .initialize()
  .then(async () => {
    const app = express();

    app.use(bodyParser.json());

    // TODO: Add logging middleware to all CRUD endpoints
    app.get(
      "/api/v1/comments/list/:nMostRecent",
      async (req: Request, res: Response) => {
        try {
          const comments: IComment[] = await dataSource.manager.find(Comment, {
            order: { dateModified: "DESC" },
            take: parseInt(req.params.nMostRecent),
          });

          // Load the posters and trips from the comment
          for (let comment of comments) {
            comment.poster = (await dataSource
              .createQueryBuilder()
              .relation(Comment, "poster")
              .of(comment)
              .loadOne())!;
            comment.trip = (await dataSource
              .createQueryBuilder()
              .relation(Comment, "trip")
              .of(comment)
              .loadOne())!;
            comment.trip.scores = await dataSource
              .createQueryBuilder()
              .relation(Trip, "scores")
              .of(comment.trip)
              .loadMany();
            comment.trip.destination = (await dataSource
              .createQueryBuilder()
              .relation(Trip, "destination")
              .of(comment.trip)
              .loadOne())!;
          }
          return res.send(comments);
        } finally {
          return res.status(500).send();
        }
      }
    );

    /** Insert new trip(s) into the database
     * Will insert trips attached to a new UserAccessScore attached to a new Comment.
     * Creates a new User with the supplied device_id if one does not already exist.
     */
    app.post("/api/v1/trips/insert", async (req: Request, res: Response) => {
      let request: IPostTripsRequest = req.body as IPostTripsRequest;

      let postResponse: { request: any; result: string; error: any } = {
        request: request,
        result: "",
        error: undefined,
      };

      if (!request.device_id) {
        postResponse.result = "Error, no ID for the user supplied";
        postResponse.error = Error("no device_id");
        return res.status(400).json(postResponse);
      } else if (!request.username) {
        postResponse.result = "Error, no name for the user supplied";
        postResponse.error = Error("no username");
        return res.status(400).json(postResponse);
      } else if (!request.scores || request.scores.length === 0) {
        postResponse.result = "Error, no scores given for this trip";
        postResponse.error = Error("no-or-empty scores");
        return res.status(400).json(postResponse);
      }

      const queryRunner = dataSource.createQueryRunner();
      await queryRunner.connect();
      await queryRunner.startTransaction();
      const manager = queryRunner.manager;

      try {
        let poster: User;
        try {
          poster = await manager.findOneByOrFail(User, {
            device_id: request.device_id,
            username: request.username,
          });
        } catch (userErr) {
          if (userErr instanceof EntityNotFoundError) {
            poster = new User();
            poster.device_id = request.device_id;
            poster.username = request.username;
            await manager.insert(User, poster);
            poster = await manager.findOneByOrFail(User, {
              device_id: request.device_id,
              username: request.username,
            });
          } else {
            postResponse.result = "Error retrieving user";
            throw userErr;
          }
        }

        // Get the intersection as requested via the origin_id of the incoming object.
        let origin: Intersection;
        try {
          origin = await manager.findOneByOrFail(Intersection, {
            intersection_id: request.origin_id,
          });
        } catch (intersectionErr) {
          if (intersectionErr instanceof EntityNotFoundError) {
            postResponse.result = "Intersection_id not found";
          } else {
            postResponse.result = "Error retrieving intersection";
          }
          throw intersectionErr;
        }

        // Get the destination object for this trip
        // We are using PostGIS to find the nearest result as the coordinates from the web application
        // do not map to the ones in the database exactly. I expect a rounding error is the cause.
        // Whatever the cause though, this fixes it by grabbing the nearest one.
        let destination: Destination | null;
        try {
          destination = await manager
            .createQueryBuilder(Destination, "destination")
            .where(
              "destination.name = :destName AND destination.type = :destType",
              {
                destName: request.destinationName,
                destType: request.destinationType,
              }
            )
            .orderBy(
              `ST_TRANSFORM(ST_SetSRID(ST_GeomFromGeoJSON('${JSON.stringify(
                request.destinationPoint
              )}'),4326), 3435) <-> geom::geometry`
            )
            .limit(1)
            .getOne();
        } catch (destinationErr) {
          if (destinationErr instanceof EntityNotFoundError) {
            postResponse.result = "Destination not found";
          } else {
            postResponse.result = "Error while retrieving destination";
          }
          throw destinationErr;
        }

        let newTrip: Trip = new Trip();
        newTrip.user = poster;
        newTrip.origin = origin;
        newTrip.destination = destination!;
        newTrip = await manager.findOneByOrFail(Trip, {
          _id: (await manager.insert(Trip, newTrip)).identifiers[0]._id,
        });

        for (let i = 0; i < request.scores.length; i++) {
          let newScore: Score = new Score();
          newScore.modeTransport = request.scores[i].modeTransport;
          newScore.score = request.scores[i].score;
          newScore.trip = newTrip;
          await manager.insert(Score, newScore);
        }

        let newComment: Comment = new Comment();
        newComment.poster = poster;
        newComment.text = request.commentText ? request.commentText : "";
        newComment.trip = newTrip;
        await manager.insert(Comment, newComment);

        await queryRunner.commitTransaction();
        postResponse.result = "Trips successfully inserted";
        res.status(201);
      } catch (err) {
        if (!postResponse.result) {
          postResponse.result = "Encountered Error while submitting trip";
        }
        postResponse.error = err;
        await queryRunner.rollbackTransaction();
        res.status(500);
        throw err;
      } finally {
        await queryRunner.release();
        return res.json(postResponse);
      }
    });

    /** Returns all trips that originate from a given origin */
    app.get("/api/v1/trips/list/:originId", (req: Request, res: Response) => {
      res.status(404).json({
        result: "Listing trips not yet implemented",
        error: "API Endpoint not Implemented",
      });
    });

    app.listen(process.env.EXPRESS_PORT || 3000);
  })
  .catch((error) => console.log(error));
