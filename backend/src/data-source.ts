import "reflect-metadata";
import { DataSource, DataSourceOptions } from "typeorm";
import { SeederOptions } from "typeorm-extension";
import { FullSeeder } from "./database/seeds/full.seeder";

import { Comment } from "./database/entity/Comment";
import { Destination } from "./database/entity/Destination";
import { Intersection } from "./database/entity/Intersection";
import { Score } from "./database/entity/Score";
import { Segment } from "./database/entity/Segment";
import { Trip } from "./database/entity/Trip";
import { User } from "./database/entity/User";
import { UserAccessScore } from "./database/view/UserAccessScore";

import commentFactory from "./database/factories/comment.factory";
import tripFactory from "./database/factories/trip.factory";
import userFactory from "./database/factories/user.factory";
import scoreFactory from "./database/factories/score.factory";

const options: DataSourceOptions & SeederOptions = {
  type: "postgres",
  host: process.env.POSTGRES_HOST,
  port: Number(process.env.POSTGRES_PORT),
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  ssl: true,
  database: process.env.POSTGRES_DB,
  synchronize: false,
  logging: false,
  entities: [
    Comment,
    Destination,
    Intersection,
    Score,
    Segment,
    Trip,
    User,
    UserAccessScore,
  ],
  migrations: [],
  subscribers: [],
  schema: process.env.POSTGRES_SCHEMA,
  seeds: [FullSeeder],
  factories: [commentFactory, scoreFactory, tripFactory, userFactory],
};

export const dataSource = new DataSource(options);
