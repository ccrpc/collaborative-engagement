import { dataSource } from "../data-source";
import { runSeeders } from "typeorm-extension";

(async () => {
    try {
        if (!dataSource.isInitialized)
            await dataSource.initialize()
        await runSeeders(dataSource)
    } catch (e: any) {
        console.log("Failed to run seeders...")
        console.error(e)
    }
})()
