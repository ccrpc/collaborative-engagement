import { setSeederFactory } from 'typeorm-extension'
import { User } from '../entity/User'

export default setSeederFactory(User, (faker) => {
    const user = new User();

    user.username = faker.name.firstName();
    user.device_id = faker.datatype.uuid();

    return user
})