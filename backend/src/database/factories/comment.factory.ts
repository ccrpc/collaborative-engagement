import { setSeederFactory } from "typeorm-extension";
import { Comment } from "../entity/Comment";
import { Faker } from "@faker-js/faker";

export default setSeederFactory(Comment, (faker: Faker) => {
  const comment = new Comment();
  comment.text = faker.lorem.text();

  return comment;
});
