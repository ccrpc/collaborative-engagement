import { setSeederFactory } from "typeorm-extension";
import { Trip } from "../entity/Trip";

export default setSeederFactory(Trip, (faker) => {
  const trip = new Trip();
  return trip;
});
