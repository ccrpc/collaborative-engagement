import { Faker } from "@faker-js/faker";
import { randomInt } from "crypto";
import { setSeederFactory } from "typeorm-extension";
import { Score } from "../entity/Score";
import { ModeTransportation } from "@ccrpc/cet-shared-lib";

export default setSeederFactory(Score, (faker: Faker) => {
  const score = new Score();

  const modeTransportationArray = [
    ModeTransportation.BIKE,
    ModeTransportation.BUS,
    ModeTransportation.DRIVE,
    ModeTransportation.WALK,
  ];

  score.modeTransport =
    modeTransportationArray[randomInt(modeTransportationArray.length)];

  score.score = faker.datatype.number({ max: 100, min: 0, precision: 1 });

  return score;
});
