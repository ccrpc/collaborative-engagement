import { ModeTransportation } from "@ccrpc/cet-shared-lib";
import { ViewEntity, ViewColumn, DataSource } from "typeorm";
import { LineString } from "geojson";
import { Score } from "../entity/Score";
import { Comment } from "../entity/Comment";

@ViewEntity({
  expression: (dataSource: DataSource) =>
    dataSource
      .createQueryBuilder()
      .select("score._id", "_id")
      .addSelect("user.username", "username")
      .addSelect("user.device_id", "device_id")
      .addSelect("score.score", "score")
      .addSelect("score.modeTransport", "transportation_mode")
      .addSelect("destination.name", "destination_name")
      .addSelect("destination.type", "destination_type")
      .addSelect("trip.dateModified", "date_modified")
      .addSelect("comment.text", "text")
      .addSelect("ST_MakeLine(origin.geom, destination.geom)", "geom")
      .from(Score, "score")
      .innerJoin("score.trip", "trip")
      .innerJoin("trip.user", "user")
      .innerJoin("trip.destination", "destination")
      .innerJoin("trip.origin", "origin")
      .innerJoin(Comment, "comment", "comment.trip = trip._id"),
})
export class UserAccessScore {
  @ViewColumn()
  username: string;

  @ViewColumn()
  device_id: string;

  @ViewColumn()
  score: number;

  @ViewColumn({ name: "transportation_mode" })
  modeTransport: ModeTransportation;

  @ViewColumn({ name: "destination_name" })
  destinationName: string;

  @ViewColumn({ name: "destination_type" })
  destinationType: string;

  @ViewColumn({ name: "date_modified" })
  dateModified: Date;

  @ViewColumn()
  text: string;

  @ViewColumn()
  geom: LineString;
}
