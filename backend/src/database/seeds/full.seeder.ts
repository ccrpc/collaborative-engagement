import { ModeTransportation } from "@ccrpc/cet-shared-lib";
import { randomInt } from "crypto";
import { DataSource } from "typeorm";
import { Seeder, SeederFactory, SeederFactoryManager } from "typeorm-extension";
import { Comment } from "../entity/Comment";
import { Destination } from "../entity/Destination";
import { Intersection } from "../entity/Intersection";
import { Score } from "../entity/Score";
import { Trip } from "../entity/Trip";
import { User } from "../entity/User";

export class FullSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager
  ): Promise<void> {
    console.log("Seeder Running");

    // The amount of each object we are going to seed
    // We do not define an amount for userAccessScores as that will equal the number of comments
    const seedAmounts: {
      users: number;
      trips: number;
    } = {
      users: 50,
      trips: 400,
    };

    const userFactory = factoryManager.get(User);
    const commentFactory = factoryManager.get(Comment);
    const scoreFactory = factoryManager.get(Score);
    const tripFactory = factoryManager.get(Trip);
    console.log("Fetching intersections");
    const intersections: Intersection[] = await dataSource
      .getRepository(Intersection)
      .find();

    console.log(`Found ${intersections.length} intersections`);

    console.log("Seeding Users");
    const users: User[] = await userFactory.saveMany(seedAmounts.users);
    console.log(`${users.length} / ${seedAmounts.users} Users Seeded`);

    console.log("Seeding Trips and comments");
    let trips: Trip[] = [];
    let scores: Score[];
    let comments: Comment[] = [];
    let selectedDestinations: Destination[] = [];
    let selectedIntersection: Intersection;
    for (let i = 0; i < seedAmounts.trips; i++) {
      selectedIntersection = intersections[randomInt(intersections.length)];
      selectedDestinations = await dataSource
        .createQueryBuilder()
        .select("destination")
        .from(Destination, "destination")
        .orderBy(
          `ST_GeomFromGeoJSON('${JSON.stringify(
            selectedIntersection.geom
          )}') <-> geom::geometry`
        )
        .limit(10)
        .getMany();

      scores = await scoresNoModeDuplicate(scoreFactory);
      const newTrip: Trip = await tripFactory.save({
        origin: selectedIntersection,
        destination:
          selectedDestinations[randomInt(selectedDestinations.length)],
        user: users[randomInt(users.length)],
        scores: scores,
      });
      const newComment: Comment = await commentFactory.save({
        poster: users[randomInt(users.length)],
        trip: newTrip,
      });

      trips.push(newTrip);
      comments.push(newComment);
    }
    console.log(
      `${trips.length} / ${seedAmounts.trips} Trips and Comments Seeded`
    );

    console.log("Seeder Finished");
  }
}

async function scoresNoModeDuplicate(
  scoreFactory: SeederFactory<Score>
): Promise<Score[]> {
  const modeArray: ModeTransportation[] = [
    ModeTransportation.WALK,
    ModeTransportation.BIKE,
    ModeTransportation.BUS,
    ModeTransportation.DRIVE,
  ];
  const numScores = randomInt(1, 4);
  let scores: Score[] = new Array<Score>(numScores);
  let modes: ModeTransportation[] = randomSubset(modeArray, numScores);
  for (let i = 0; i < numScores; i++) {
    scores.push(await scoreFactory.save({ modeTransport: modes[i] }));
  }
  return scores;
}

function randomSubset<T>(inputArray: T[], n: number): T[] {
  let result: T[] = new Array<T>(n);
  let len: number = inputArray.length;
  let taken: number[] = new Array<number>(len);
  if (n > len) {
    throw new RangeError("randomSubset: more elements taken than available");
  }
  while (n--) {
    let x: number = Math.floor(Math.random() * len);
    result[n] = inputArray[x in taken ? taken[x] : x];
    taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
}
