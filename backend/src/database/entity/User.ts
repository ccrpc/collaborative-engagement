import { IUser, IComment, ITrip } from "@ccrpc/cet-shared-lib/models";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  Unique,
} from "typeorm";
import { Comment } from "./Comment";
import { Trip } from "./Trip";
@Entity()
@Unique(["username", "device_id"])
export class User implements IUser {
  @PrimaryGeneratedColumn()
  _id: number;

  @Column()
  username: string;

  @Column()
  device_id: string;

  @OneToMany(() => Comment, (comment) => comment.poster)
  comments: IComment[];

  @OneToMany(() => Trip, (trip) => trip.user)
  trips: ITrip[];
}
