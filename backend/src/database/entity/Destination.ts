import { Column, Entity, Index, PrimaryGeneratedColumn, Unique } from "typeorm";
import { IDestination, DestinationType } from "@ccrpc/cet-shared-lib";
import { Point } from "geojson";

@Entity()
@Unique(["geom", "type", "name"])
export class Destination implements IDestination {
  @PrimaryGeneratedColumn()
  _id: number;

  @Index({ spatial: true })
  @Column({
    type: "geometry",
    spatialFeatureType: "Point",
    srid: 3435,
  })
  geom: Point;

  @Column({
    type: "enum",
    enum: [
      DestinationType.ARTS_ENTERTAINMENT,
      DestinationType.GROCERY,
      DestinationType.HEALTH,
      DestinationType.JOB,
      DestinationType.PARK,
      DestinationType.PUBLIC_FACILITY,
      DestinationType.RESTAURANT,
      DestinationType.RETAIL,
      DestinationType.SCHOOL,
      DestinationType.SERVICE,
    ],
  })
  type: string;

  @Column({ nullable: true, type: "character varying" })
  category: string | null;

  @Column()
  name: string;
}
