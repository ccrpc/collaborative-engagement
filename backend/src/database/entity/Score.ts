import { ModeTransportation, ITrip, IScore } from "@ccrpc/cet-shared-lib";
import {
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
} from "typeorm";
import { Trip } from "./Trip";

@Entity()
@Unique(["modeTransport", "trip"])
export class Score implements IScore {
  @PrimaryGeneratedColumn()
  _id: number;

  @Column({
    type: "enum",
    enum: [
      ModeTransportation.WALK,
      ModeTransportation.BIKE,
      ModeTransportation.BUS,
      ModeTransportation.DRIVE,
    ],
    name: "transportation_mode",
  })
  modeTransport: ModeTransportation;

  @Column()
  score: number;

  @ManyToOne(() => Trip, (trip) => trip.scores)
  trip: ITrip;
}
