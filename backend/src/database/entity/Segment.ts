import { ISegment } from "@ccrpc/cet-shared-lib";
import { Column, Entity, PrimaryGeneratedColumn, Index } from "typeorm";
import { LineString } from "geojson";

@Entity()
export class Segment implements ISegment {
  @PrimaryGeneratedColumn()
  _id: number;

  @Index({ spatial: true })
  @Column({
    type: "geometry",
    spatialFeatureType: "LineString",
    srid: 3435,
  })
  geom: LineString;

  @Column({ unique: true })
  segment_id: number;

  @Column({ nullable: true, type: "character varying" })
  name: string | null;

  @Column({ nullable: true, type: "character varying" })
  cross_name_start: string | null;

  @Column({ nullable: true, type: "character varying" })
  cross_name_end: string | null;

  // This, similar to intersection, lists the ease-of-access of different destination types via different modes of transport.
  // The difference being that the values in the segment can be null.
  // This is fine though, as we do not use this information in the project.

  // BUS
  // The ease with which it is to take the bus to different location types.
  // A number between 0 and 100, 0 for being very hard and 100 for being very easy.
  @Column({ nullable: true, type: "double precision" })
  bus_art: number | null;

  @Column({ nullable: true, type: "double precision" })
  bus_grocery: number | null;

  @Column({ nullable: true, type: "double precision" })
  bus_health: number | null;

  @Column({ nullable: true, type: "double precision" })
  bus_job: number | null;

  @Column({ nullable: true, type: "double precision" })
  bus_park: number | null;

  @Column({ nullable: true, type: "double precision" })
  bus_public: number | null;

  @Column({ nullable: true, type: "double precision" })
  bus_restaurant: number | null;

  @Column({ nullable: true, type: "double precision" })
  bus_retail: number | null;

  @Column({ nullable: true, type: "double precision" })
  bus_school: number | null;

  @Column({ nullable: true, type: "double precision" })
  bus_service: number | null;

  // PEDESTRIAN
  // The ease with which it is to wak to different location types
  // A number between 0 and 100, 0 for being very hard and 100 for being very easy.
  @Column({ nullable: true, type: "double precision" })
  pedestrian_art: number | null;

  @Column({ nullable: true, type: "double precision" })
  pedestrian_grocery: number | null;

  @Column({ nullable: true, type: "double precision" })
  pedestrian_health: number | null;

  @Column({ nullable: true, type: "double precision" })
  pedestrian_job: number | null;

  @Column({ nullable: true, type: "double precision" })
  pedestrian_park: number | null;

  @Column({ nullable: true, type: "double precision" })
  pedestrian_public: number | null;

  @Column({ nullable: true, type: "double precision" })
  pedestrian_restaurant: number | null;

  @Column({ nullable: true, type: "double precision" })
  pedestrian_retail: number | null;

  @Column({ nullable: true, type: "double precision" })
  pedestrian_school: number | null;

  @Column({ nullable: true, type: "double precision" })
  pedestrian_service: number | null;

  // BICYCLE
  // The ease with which it is to bike to different location types
  // A number between 0 and 100, 0 for being very hard and 100 for being very easy.
  @Column({ nullable: true, type: "double precision" })
  bicycle_art: number | null;

  @Column({ nullable: true, type: "double precision" })
  bicycle_grocery: number | null;

  @Column({ nullable: true, type: "double precision" })
  bicycle_health: number | null;

  @Column({ nullable: true, type: "double precision" })
  bicycle_job: number | null;

  @Column({ nullable: true, type: "double precision" })
  bicycle_park: number | null;

  @Column({ nullable: true, type: "double precision" })
  bicycle_public: number | null;

  @Column({ nullable: true, type: "double precision" })
  bicycle_restaurant: number | null;

  @Column({ nullable: true, type: "double precision" })
  bicycle_retail: number | null;

  @Column({ nullable: true, type: "double precision" })
  bicycle_school: number | null;

  @Column({ nullable: true, type: "double precision" })
  bicycle_service: number | null;

  // VEHICLE
  // The ease with which it is to drive to different location types
  // A number between 0 and 100, 0 for being very hard and 100 for being very easy.
  @Column({ nullable: true, type: "double precision" })
  vehicle_art: number | null;

  @Column({ nullable: true, type: "double precision" })
  vehicle_grocery: number | null;

  @Column({ nullable: true, type: "double precision" })
  vehicle_health: number | null;

  @Column({ nullable: true, type: "double precision" })
  vehicle_job: number | null;

  @Column({ nullable: true, type: "double precision" })
  vehicle_park: number | null;

  @Column({ nullable: true, type: "double precision" })
  vehicle_public: number | null;

  @Column({ nullable: true, type: "double precision" })
  vehicle_restaurant: number | null;

  @Column({ nullable: true, type: "double precision" })
  vehicle_retail: number | null;

  @Column({ nullable: true, type: "double precision" })
  vehicle_school: number | null;

  @Column({ nullable: true, type: "double precision" })
  vehicle_service: number | null;
}
