import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  OneToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
} from "typeorm";
import { User } from "./User";
import { IComment, ITrip } from "@ccrpc/cet-shared-lib/models";
import { Trip } from "./Trip";

@Entity()
export class Comment implements IComment {
  @PrimaryGeneratedColumn()
  _id: number;

  @Column()
  text: string;

  @CreateDateColumn({ name: "date_published" })
  datePublished: Date;

  @UpdateDateColumn({ name: "date_modified" })
  dateModified: Date;

  @ManyToOne(() => User, (user) => user.comments)
  poster: User;

  @OneToOne(() => Trip)
  @JoinColumn()
  trip: ITrip;
}
