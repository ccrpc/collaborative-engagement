import { IIntersection } from "@ccrpc/cet-shared-lib";
import { Point } from "geojson";
import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

/**
 * This entity represents the Intersection object created by the {@link https://gitlab.com/ccrpc/cuuats.snt.accessibility accessibility script}.
 *
 * For specifics on how each categories are calculated, see the accessibility script gitlab README.
 */
@Entity()
export class Intersection implements IIntersection {
  @PrimaryGeneratedColumn()
  _id: number;

  @Index({ spatial: true })
  @Column({
    type: "geometry",
    spatialFeatureType: "Point",
    srid: 3435,
  })
  geom: Point;

  @Column({ unique: true })
  intersection_id: number;

  // BUS
  // The ease with which it is to take the bus to different location types.
  // A number between 0 and 100, 0 for being very hard and 100 for being very easy.
  @Column()
  bus_art: number;

  @Column()
  bus_grocery: number;

  @Column()
  bus_health: number;

  @Column()
  bus_job: number;

  @Column()
  bus_park: number;

  @Column()
  bus_public: number;

  @Column()
  bus_restaurant: number;

  @Column()
  bus_retail: number;

  @Column()
  bus_school: number;

  @Column()
  bus_service: number;

  // PEDESTRIAN
  // The ease with which it is to wak to different location types
  // A number between 0 and 100, 0 for being very hard and 100 for being very easy.
  @Column()
  pedestrian_art: number;

  @Column()
  pedestrian_grocery: number;

  @Column()
  pedestrian_health: number;

  @Column()
  pedestrian_job: number;

  @Column()
  pedestrian_park: number;

  @Column()
  pedestrian_public: number;

  @Column()
  pedestrian_restaurant: number;

  @Column()
  pedestrian_retail: number;

  @Column()
  pedestrian_school: number;

  @Column()
  pedestrian_service: number;

  // BICYCLE
  // The ease with which it is to bike to different location types
  // A number between 0 and 100, 0 for being very hard and 100 for being very easy.
  @Column()
  bicycle_art: number;

  @Column()
  bicycle_grocery: number;

  @Column()
  bicycle_health: number;

  @Column()
  bicycle_job: number;

  @Column()
  bicycle_park: number;

  @Column()
  bicycle_public: number;

  @Column()
  bicycle_restaurant: number;

  @Column()
  bicycle_retail: number;

  @Column()
  bicycle_school: number;

  @Column()
  bicycle_service: number;

  // VEHICLE
  // The ease with which it is to drive to different location types
  // A number between 0 and 100, 0 for being very hard and 100 for being very easy.
  @Column()
  vehicle_art: number;

  @Column()
  vehicle_grocery: number;

  @Column()
  vehicle_health: number;

  @Column()
  vehicle_job: number;

  @Column()
  vehicle_park: number;

  @Column()
  vehicle_public: number;

  @Column()
  vehicle_restaurant: number;

  @Column()
  vehicle_retail: number;

  @Column()
  vehicle_school: number;

  @Column()
  vehicle_service: number;
}
