import {
  IComment,
  ITrip,
  IIntersection,
  IUser,
  IDestination,
  IScore,
} from "@ccrpc/cet-shared-lib/models";
import {
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
  JoinColumn,
  OneToOne,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from "typeorm";
import { Comment } from "./Comment";
import { Destination } from "./Destination";
import { Intersection } from "./Intersection";
import { User } from "./User";
import { Score } from "./Score";

@Entity()
export class Trip implements ITrip {
  @PrimaryGeneratedColumn()
  _id: number;

  @CreateDateColumn({ name: "date_created" })
  dateCreated: Date;

  @UpdateDateColumn({ name: "date_modified" })
  dateModified: Date;

  @ManyToOne(() => User, (user) => user.trips)
  user: IUser;

  @OneToOne(() => Comment)
  comment: IComment;

  @JoinColumn({ referencedColumnName: "intersection_id" })
  @ManyToOne(() => Intersection)
  origin: IIntersection;

  @JoinColumn({ referencedColumnName: "_id" })
  @ManyToOne(() => Destination)
  destination: IDestination;

  @OneToMany(() => Score, (score) => score.trip)
  scores: IScore[];
}
