CREATE TABLE access_score AS
SELECT fid,
      (bus_grocery + bus_art + bus_restaurant + bus_job +
      bus_retail + bus_service + bus_park + bus_health +
      bus_public + bus_school) / 10 as bus_score,
      (pedestrian_grocery + pedestrian_art + pedestrian_restaurant +
      pedestrian_job + pedestrian_retail + pedestrian_service +
      pedestrian_park + pedestrian_health + pedestrian_public +
      pedestrian_school) / 10 as pedestrian_score,
      (bicycle_grocery + bicycle_art + bicycle_restaurant + bicycle_job +
      bicycle_retail + bicycle_service + bicycle_park + bicycle_health +
      bicycle_public + bicycle_school) / 10 as bicycle_score,
      (vehicle_grocery + vehicle_art + vehicle_restaurant + vehicle_job +
      vehicle_retail + vehicle_service + vehicle_park + vehicle_health +
      vehicle_public + vehicle_school) / 10 as vehicle_score,
      wkb_geometry as geometry
FROM intersection
